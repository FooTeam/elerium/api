<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory()->create(['username' => 'jsmith', 'email' => 'jsmith@dev.footeam.fr']);
        User::factory()->create(['username' => 'jane_doe']);
        User::factory()->count(5)->create();
    }
}
