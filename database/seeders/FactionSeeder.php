<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FactionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (['federation', 'empire', 'conglomerate'] as $name) {
            DB::table('factions')->insert(['name' => $name]);
        }
    }
}
