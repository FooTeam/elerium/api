<?php

namespace Database\Seeders;

use App\Models\Stargate;
use Illuminate\Database\Seeder;

class StargateSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        Stargate::addStargatesToPlanet();
    }
}
