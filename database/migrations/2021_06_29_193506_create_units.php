<?php

use App\GameRules\GameService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @see \App\Models\Unit
     */
    public function up(): void
    {
        Schema::create('units', function (Blueprint $table): void {
            $table->id();
            $table->enum('type', GameService::getCodeNames(GameService::UNITS));
            $table->integer('quantity');
            // If in a fleet
            $table->foreignId('fleet_id')->nullable()->constrained();
            // If in a district
            $table->foreignId('district_id')->nullable()->constrained();
            // Si in a stargate
            $table->foreignId('stargate_id')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('units');
    }
}
