<?php

use App\GameRules\GameService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('district_fields', function (Blueprint $table): void {
            $table->primary(['x', 'y', 'district_id']);
            $table->unsignedSmallInteger('x');
            $table->unsignedSmallInteger('y');
            $table->foreignId('district_id')->constrained();
            $table->enum('value', GameService::getDistrictFieldsCodeNames());
            // null if not building
            $table->unsignedSmallInteger('level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('district_fields');
    }
}
