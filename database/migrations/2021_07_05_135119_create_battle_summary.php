<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattleSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_summaries', function (Blueprint $table) {
            $table->id();
            $table->boolean('win')->nullable();
            $table->foreignId('district_id')->constrained('districts');
            $table->foreignId('stargate_id')->constrained('stargates');
            $table->timestamps();
        });

        Schema::table('units', function ($table) {
            // If in a battle summary
            $table->foreignId('battle_summary_id')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_summary');
        Schema::table('units', function ($table) {
            $table->dropColumn('battle_summary_id');
        });
    }
}
