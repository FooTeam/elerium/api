<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('district_resources', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('district_id')->constrained();
            $table->enum('resource', ['metal', 'graphite', 'elerium', 'paste', 'science']);
            $table->integer('last_quantity_updated')->default(0);
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('district_resources');
    }
}
