<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateStargatesTable.
 * @see \App\Models\Stargate
 */
class CreateStargatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('stargates', function (Blueprint $table): void {
            $table->id();
            $table->integer('x');
            $table->integer('y');
            $table->enum('color', ['yellow', 'orange', 'violet', 'red']);
            $table->foreignId('user_id')->nullable()->constrained();
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('stargates');
    }
}
