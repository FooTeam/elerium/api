<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('factions', function (Blueprint $table): void {
            $table->enum('name', ['federation', 'empire', 'conglomerate'])->primary();
            foreach (['yellow', 'orange', 'violet', 'red'] as $color) {
                $table->integer($color)->default(0);
            }
            $table->timestamp('updated_at')->default('now');
        });

        Schema::table('users', function (Blueprint $table): void {
            $table->string('faction');
            $table->foreign('faction')->references('name')->on('factions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('factions');
        Schema::table('users', function (Blueprint $table): void {
            $table->dropColumn(['faction']);
        });
    }
}
