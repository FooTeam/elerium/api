<?php

namespace Database\Factories;

use App\Models\Faction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    public function configure()
    {
        return $this->afterCreating(function (User $user): void {
            Cache::increment('player-count');
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->unique()->regexify('[A-Za-z0-9-_]{'.random_int(2, 18).'}'),
            'faction' => Faction::NAMES[array_rand(Faction::NAMES)],
            'email' => $this->faker->unique()->optional()->safeEmail,
            'email_verified_at' => now(),
            'password' => 'password',
            'remember_token' => Str::random(10),
        ];
    }
}
