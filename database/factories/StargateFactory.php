<?php

namespace Database\Factories;

use App\GameRules\Planet;
use App\Models\Stargate;
use Illuminate\Database\Eloquent\Factories\Factory;

class StargateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stargate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'x' => random_int(0, Planet::SIZE),
            'y' => random_int(0, Planet::SIZE),
            'color' => array_rand(Stargate::COLORS),
        ];
    }
}
