<?php

namespace App\GameRules;

use App\GameRules\Researches\AbstractResearch;
use App\Http\Resources\Research as ResearchResource;
use App\Models\Research;

class GameService
{
    public const PRODUCTION_BUILDINGS = [
        Buildings\EleriumAccelerator::class,
        Buildings\HydroponicFields::class,
        Buildings\FoodProcessingPlant::class,
        Buildings\GraphiteMine::class,
        Buildings\MetalMine::class,
        Buildings\Labs::class,
    ];

    public const STORAGE_BUILDINGS = [
        Buildings\EleriumStorage::class,
        Buildings\PasteStorage::class,
        Buildings\GraphiteStorage::class,
        Buildings\MetalStorage::class,
    ];

    public const BUILDINGS = [
        ...self::PRODUCTION_BUILDINGS,
        ...self::STORAGE_BUILDINGS,
        Buildings\DistrictCouncil::class,
        Buildings\Labs::class,
        Buildings\Starshipyard::class,
    ];

    public const RESEARCHES = [
        ...self::PRODUCTION_RESEARCHES,
        ...self::STORAGE_RESEARCHES,
        ...self::NATURAL_RESEARCHES,
        Researches\AdvancedBuildingsResearch::class,
        Researches\Storage::class,
        Researches\FoodProcessingPlant::class,
        Researches\Starshipyard::class,
        Researches\EleriumAccelerator::class,
    ];

    public const PRODUCTION_RESEARCHES = [
        Researches\Production\DeepDrilling::class,
    ];

    public const STORAGE_RESEARCHES = [
        Researches\Storage\StackingAlgorithm::class,
        Researches\Storage\DeepGeologicalStorage::class,
    ];

    public const NATURAL_RESEARCHES = [
        Researches\Natural\GroundScanner::class,
        Researches\Natural\HighSpeedParticulesCompressor::class,
        Researches\Natural\SoilEnrichment::class,
    ];

    public const NATURALS = [
        Naturals\FertileSoil::class,
        Naturals\GraphiteDeposit::class,
        Naturals\MetalDeposit::class,
        Naturals\MeteoriteImpact::class,
    ];

    public const UNITS = [
        Units\AttackShip::class,
        Units\DefenseShip::class,
        Units\PolyvalentShip::class,
        Units\Speeder::class,
    ];

    public static function getCodeNames(array $rules): array
    {
        return array_map(fn ($classFQN) => $classFQN::codeName(), $rules);
    }

    /**
     * Theses are the possible values of a field in a district.
     * An empty field don't have a name, it's just empty.
     * @return string[]
     */
    public static function getDistrictFieldsCodeNames(): array
    {
        return self::getCodeNames(array_merge(self::BUILDINGS, self::NATURALS));
    }

    /**
     * @return string[]
     */
    public static function getResearchesCodeNames(): array
    {
        return self::getCodeNames(self::RESEARCHES);
    }

    public static function getProductionResearchesCodeNames(): array
    {
        return self::getCodeNames(self::PRODUCTION_RESEARCHES);
    }

    public static function getStorageResearchesCodeNames(): array
    {
        return self::getCodeNames(self::STORAGE_RESEARCHES);
    }

    public function getResearchTree(string $firstNode = Researches\EleriumAccelerator::class): ResearchResource
    {
        /** @var AbstractResearch $rule */
        $rule = new $firstNode;

        $model = new Research(['name' => $rule->codeName()]);
        $resource = new ResearchResource($model);
        $resource->additional(['unlocks' => array_map(fn ($unlocked) => $this->getResearchTree($unlocked), $rule->unlockedResearches())]);

        return $resource;
    }
}
