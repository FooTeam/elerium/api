<?php

namespace App\GameRules\Naturals;

class MeteoriteImpact extends Natural
{
    /**
     * {@inheritdoc}
     */
    public static function getProbabilities(): array
    {
        return ['frequency' => 1, 'probability' => 1];
    }
}
