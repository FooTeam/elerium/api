<?php

namespace App\GameRules\Naturals;

use App\Traits\HasCodeName;

abstract class Natural
{
    use HasCodeName;

    /**
     * The probabilities to get this natural on a new District (in %).
     * @return int[] ['frequency' => int, 'probability' => int, 'next' => ?int]
     */
    abstract public static function getProbabilities(): array;
}
