<?php

namespace App\GameRules\Naturals;

class FertileSoil extends Natural
{
    /**
     * {@inheritdoc}
     */
    public static function getProbabilities(): array
    {
        return ['frequency' => 2, 'probability' => 4, 'next' => 24];
    }
}
