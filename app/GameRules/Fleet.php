<?php

namespace App\GameRules;

use App\Models\Fleet as ModelsFleet;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class Fleet
{
    private const ELERIUM_CONSUMPTION = 20;
    private const PASTE_CONSUMPTION = 5;

    public static function consumeResource(ModelsFleet $fleet): void
    {
        $errors = [];
        $messages = [];

        $distance = Planet::getDistanceBetweenFields($fleet->from->x, $fleet->from->y, $fleet->to->x, $fleet->to->y);
        $nbUnits = $fleet->units()->get()->reduce(fn ($quantity, $units) => $quantity+$units->quantity);
        $time = $fleet->getTravelTime();
        $eleriumConsumption = $distance * self::ELERIUM_CONSUMPTION * $nbUnits;
        $pasteConsumption = $time * self::PASTE_CONSUMPTION * $nbUnits;

        $resources = DistrictResources::getArray($fleet->from->id);

        if ($resources['elerium']['value'] < $eleriumConsumption) {
            $errors[] = 'elerium';
            $messages[] = "{$eleriumConsumption} elerium needed, this district got {$resources['elerium']['value']}.";
        }
        if ($resources['paste']['value'] < $pasteConsumption) {
            $errors[] = 'paste';
            $messages[] = "{$pasteConsumption} paste needed, this district got {$resources['paste']['value']}.";
        }

        if ([] !== $errors) {
            throw new HttpResponseException(response()->json(['errors' => $errors, 'messages' => $messages], JsonResponse::HTTP_BAD_REQUEST));
        }
        
        $fleet->from->resources()->where('resource', 'elerium')->first()->consume($eleriumConsumption);
        $fleet->from->resources()->where('resource', 'paste')->first()->consume($pasteConsumption);
    }
}
