<?php

namespace App\GameRules\Units;

use App\GameRules\GameService;

class UnitFactory
{
    /**
     * @return AbstractUnit|null return null if the type is unknown
     */
    public static function create(string $type): AbstractUnit
    {
        foreach (GameService::UNITS as $unitName) {
            if (str_contains($unitName, $type)) {
                return new $unitName;
            }
        }
        return null;
    }
}
