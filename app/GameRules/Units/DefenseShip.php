<?php

namespace App\GameRules\Units;

class DefenseShip extends AbstractUnit
{
    public function __construct()
    {
        parent::__construct(5, 10);
    }

    public function neededMetal(): int
    {
        return 350;
    }

    public function neededElerium(): int
    {
        return 20;
    }

    public function getTravelSpeed(): int
    {
        return 5;
    }
}
