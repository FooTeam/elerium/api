<?php

namespace App\GameRules\Units;

class PolyvalentShip extends AbstractUnit
{
    public function __construct()
    {
        parent::__construct(8, 8);
    }

    public function neededMetal(): int
    {
        return 150;
    }

    public function neededElerium(): int
    {
        return 20;
    }

    public function getTravelSpeed(): int
    {
        return 8;
    }
}
