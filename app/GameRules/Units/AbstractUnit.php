<?php

namespace App\GameRules\Units;

use App\GameRules\DistrictResources;
use App\GameRules\UserManageable;
use App\Models\District;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


abstract class AbstractUnit extends UserManageable
{
    public int $attack;
    public int $defense;

    protected function __construct(int $attack, int $defense)
    {
        $this->attack = $attack;
        $this->defense = $defense;
    }

    public function getAttack(): int
    {
        return $this->attack;
    }

    public function getDefense(): int
    {
        return $this->defense;
    }

    /**
     * @return int tiles/hour
     */
    abstract public function getTravelSpeed(): int;

    /**
     * TODO: ??
     */
    public function neededResearch(): ?string
    {
        return null;
    }

    abstract public function neededMetal(): int;

    abstract public function neededElerium(): int;

    public function neededPaste(): int
    {
        return 30;
    }

    public function consumeResources(District $district, $quantity)
    {
        $errors = [];
        $messages = [];

        $resources = DistrictResources::getArray($district->id);

        $neededMetal = $this->neededMetal() * $quantity;
        $neededElerium = $this->neededElerium() * $quantity;
        $neededPaste = $this->neededPaste() * $quantity;

        if ($resources['metal']['value'] < $neededMetal) {
            $errors[] = 'metal';
            $messages[] = "{$neededMetal} metal needed, this district got {$resources['metal']['value']}.";
        }
        if ($resources['elerium']['value'] < $neededElerium) {
            $errors[] = 'elerium';
            $messages[] = "{$neededElerium} elerium needed, this district got {$resources['elerium']['value']}.";
        }
        if ($resources['paste']['value'] < $neededPaste) {
            $errors[] = 'paste';
            $messages[] = "{$neededPaste} paste needed, this district got {$resources['paste']['value']}.";
        }

        if ([] !== $errors) {
            throw new HttpResponseException(response()->json(['errors' => $errors, 'messages' => $messages], JsonResponse::HTTP_BAD_REQUEST));
        }

        $district->resources()->where('resource', 'metal')->first()->consume($neededMetal);
        $district->resources()->where('resource', 'elerium')->first()->consume($neededElerium);
        $district->resources()->where('resource', 'paste')->first()->consume($neededPaste);
    }

    public function toArray(): array
    {
        return [
            'type' => self::codeName(),
            'speed' => $this->getTravelSpeed(),
            'defense' => $this->getDefense(),
            'attack' => $this->getAttack(),
        ];
    }
}
