<?php

namespace App\GameRules\Units;

/**
 * @TODO remove values for demo
 */
class Speeder extends AbstractUnit
{
    public function __construct()
    {
        parent::__construct(100, 100);
//        parent::__construct(1, 1);
    }

    public function neededMetal(): int
    {
        return 1;
//        return 20;
    }

    public function neededElerium(): int
    {
        return 1;
//        return 10;
    }

    // TODO: remove
    public function neededPaste(): int
    {
        return 1;
    }

    public function getTravelSpeed(): int
    {
        return 1000;
//        return 16;
    }
}
