<?php

namespace App\GameRules\Units;

class AttackShip extends AbstractUnit
{

    public function __construct()
    {
        parent::__construct(10, 5);
    }

    public function neededMetal(): int
    {
        return 150;
    }

    public function neededElerium(): int
    {
        return 20;
    }

    public function getTravelSpeed(): int
    {
        return 7;
    }
}
