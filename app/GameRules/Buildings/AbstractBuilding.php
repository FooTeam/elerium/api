<?php

namespace App\GameRules\Buildings;

use App\GameRules\DistrictResources;
use App\GameRules\UserManageable;
use App\Models\District;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

abstract class AbstractBuilding extends UserManageable
{
    public int $level;

    public function __construct(int $level)
    {
        $this->level = $level;
    }

    abstract public function neededMetal(): int;

    abstract public function neededGraphite(): int;

    /**
     * @throw HttpResponseException
     */
    public function validateUpgradeAndConsumeResources(District $district): void
    {
        $errors = [];
        $messages = [];

        if (DistrictCouncil::class !== get_called_class() && $district->level() < $this->level + 1) {
            $errors[] = 'district';
            $messages[] = 'Upgrade your District Council before.';
        }

        $resources = DistrictResources::getArray($district->id);

        if ($resources['metal']['value'] < $this->neededMetal()) {
            $errors[] = 'metal';
            $messages[] = "{$this->neededMetal()} metal needed, this district got {$resources['metal']['value']}.";
        }

        if ($resources['graphite']['value'] < $this->neededGraphite()) {
            $errors[] = 'graphite';
            $messages[] = "{$this->neededGraphite()} graphite needed, this district got {$resources['graphite']['value']}.";
        }

        if ([] !== $errors) {
            throw new HttpResponseException(response()->json(['errors' => $errors, 'messages' => $messages], JsonResponse::HTTP_BAD_REQUEST));
        }

        $district->resources()->where('resource', 'metal')->first()->consume($this->neededMetal());
        $district->resources()->where('resource', 'graphite')->first()->consume($this->neededGraphite());
    }

    /**
     * Most of the buildings don't need a research
     * that's why it default at null.
     */
    public function neededResearch(): ?string
    {
        return null;
    }
}
