<?php

namespace App\GameRules\Buildings;

use App\GameRules\GameService;
use App\GameRules\Researches\AbstractResearch;
use App\GameRules\Researches\Production\ProductionResearch;
use App\Models\Research;

abstract class AbstractProductionBuilding extends AbstractBuilding
{
    public const NATURAL_BOOST = 0.25;

    /**
     * Should return one of the following :
     * FertileSoil::codeName(), GraphiteDeposit::codeName(), MetalDeposit::codeName(), MeteoriteImpact::codeName().
     * @return string|null
     */
    abstract public function getRelatedNatural(): ?string;

    /**
     * Should return one of the following :
     * metal, graphite, elerium, paste.
     * @return string
     */
    abstract public function getResourceName(): string;

    final public function baseProduction(): int
    {
        return $this->production($this->level);
    }

    public function totalProduction(int $numberOfNaturals = 0, ?int $level = null): int
    {
        $boost = $this->getResearchBoost() + ($numberOfNaturals > 0 ? $this->getBoostPerNaturals() * $numberOfNaturals : 0);

        return $this->production($level ?: $this->level) * (1 + $boost);
    }

    /**
     * @return float|int get the boost given from 1 naturals
     */
    public function getBoostPerNaturals(): float | int
    {
        if (null === $this->getRelatedNatural()) {
            return 0;
        }

        $boost = self::NATURAL_BOOST;
        Research::getPlayerResearches()
            ->whereIn('name', GameService::getCodeNames(GameService::NATURAL_RESEARCHES))
            ->each(function (Research $model) use (&$boost): void {
                $className = AbstractResearch::getRuleFQDN($model->name);
                $research = new $className;
                if (in_array($this->getRelatedNatural(), $research->natural())) {
                    $boost *= 1 + $research->boost();
                }
            });

        return $boost;
    }

    public function getResearchBoost(): float | int
    {
        $researchBoost = 0;
        Research::getPlayerResearches()
            ->whereIn('name', GameService::getProductionResearchesCodeNames())
            ->each(function (Research $model) use (&$researchBoost): void {
                $className = AbstractResearch::getRuleFQDN($model->name);
                $research = new $className;
                if (
                    is_subclass_of($research, ProductionResearch::class)
                    && in_array($this->getResourceName(), $research->resources())
                ) {
                    $researchBoost += $research->boost();
                }
            });

        return $researchBoost;
    }

    abstract protected function production(int $level): int;
}
