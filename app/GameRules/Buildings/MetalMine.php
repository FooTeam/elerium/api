<?php

namespace App\GameRules\Buildings;

use App\GameRules\Naturals\MetalDeposit;

class MetalMine extends AbstractProductionBuilding
{
    public function getRelatedNatural(): string
    {
        return MetalDeposit::codeName();
    }

    public function getResourceName(): string
    {
        return 'metal';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    protected function production(int $level): int
    {
        return $level * 120;
    }
}
