<?php

namespace App\GameRules\Buildings;

class DistrictCouncil extends AbstractBuilding
{
    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }
}
