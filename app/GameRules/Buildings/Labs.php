<?php

namespace App\GameRules\Buildings;

use App\Models\DistrictResource;

class Labs extends AbstractProductionBuilding
{
    public function getRelatedNatural(): ?string
    {
        return null;
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    public function production(int $level): int
    {
        return 1 * $level;
    }

    public function getResourceName(): string
    {
        return 'science';
    }
}
