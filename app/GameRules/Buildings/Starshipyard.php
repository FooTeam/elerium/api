<?php

namespace App\GameRules\Buildings;

use App\GameRules\Researches\Starshipyard as Research;

class Starshipyard extends AbstractBuilding
{
    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    public function neededResearch(): string
    {
        return Research::class;
    }

    public function unitStorage(?int $level = null): int
    {
        if (null === $level) {
            $level = $this->level;
        }

        return $level ** 1.2 * 8;
    }
}
