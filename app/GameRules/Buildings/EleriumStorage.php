<?php

namespace App\GameRules\Buildings;

class EleriumStorage extends AbstractStorageBuilding
{
    public function getResourceName(): string
    {
        return 'elerium';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    protected function capacity($level): int
    {
        return $level * 1000;
    }
}
