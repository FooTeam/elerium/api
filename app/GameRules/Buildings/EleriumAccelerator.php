<?php

namespace App\GameRules\Buildings;

use App\GameRules\Naturals\MeteoriteImpact;
use App\GameRules\Researches\EleriumAccelerator as Research;

class EleriumAccelerator extends AbstractProductionBuilding
{
    public function getRelatedNatural(): string
    {
        return MeteoriteImpact::codeName();
    }

    public function getResourceName(): string
    {
        return 'elerium';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    public function neededResearch(): string
    {
        return Research::class;
    }

    protected function production(int $level): int
    {
        return $level * 120;
    }
}
