<?php

namespace App\GameRules\Buildings;

use App\GameRules\Naturals\GraphiteDeposit;

class GraphiteMine extends AbstractProductionBuilding
{
    public function getRelatedNatural(): string
    {
        return GraphiteDeposit::codeName();
    }

    public function getResourceName(): string
    {
        return 'graphite';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    protected function production(int $level): int
    {
        return $level * 120;
    }
}
