<?php

namespace App\GameRules\Buildings;

use App\GameRules\GameService;
use App\GameRules\Researches\AbstractResearch;
use App\GameRules\Researches\Storage\StorageResearch;
use App\Models\Research;

abstract class AbstractStorageBuilding extends AbstractBuilding
{
    /**
     * Should return one of the following :
     * metal, graphite, elerium, paste.
     * @return string
     */
    abstract public function getResourceName(): string;

    final public function baseCapacity(): int
    {
        return $this->capacity($this->level);
    }

    public function totalStorage(?int $level = null): int
    {
        return $this->capacity($level ?: $this->level) * (1 + $this->getResearchBoost());
    }

    public function getResearchBoost(): float | int
    {
        $researchBoost = 0;
        Research::getPlayerResearches()
            ->whereIn('name', GameService::getStorageResearchesCodeNames())
            ->each(function (Research $model) use (&$researchBoost): void {
                $className = AbstractResearch::getRuleFQDN($model->name);
                $research = new $className;
                if (is_subclass_of($research, StorageResearch::class)) {
                    $researchBoost += $research->boost();
                }
            });

        return $researchBoost > 0 ? $researchBoost : 1;
    }

    abstract protected function capacity(int $level): int;
}
