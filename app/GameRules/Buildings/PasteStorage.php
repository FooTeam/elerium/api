<?php

namespace App\GameRules\Buildings;

class PasteStorage extends AbstractStorageBuilding
{
    public function getResourceName(): string
    {
        return 'paste';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    protected function capacity($level): int
    {
        return $level * 1000;
    }
}
