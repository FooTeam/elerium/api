<?php

namespace App\GameRules\Buildings;

use App\GameRules\Naturals\FertileSoil;
use App\GameRules\Researches\FoodProcessingPlant as Research;

class FoodProcessingPlant extends AbstractProductionBuilding
{
    public function getRelatedNatural(): string
    {
        return FertileSoil::codeName();
    }

    public function getResourceName(): string
    {
        return 'paste';
    }

    public function neededGraphite(): int
    {
        return $this->level * 10;
    }

    public function neededMetal(): int
    {
        return $this->level * 10;
    }

    public function neededResearch(): string
    {
        return Research::class;
    }

    protected function production(int $level): int
    {
        return $level * 120;
    }
}
