<?php

namespace App\GameRules;

use App\GameRules\Buildings\AbstractProductionBuilding;
use App\GameRules\Buildings\AbstractStorageBuilding;
use App\Models\DistrictField;
use App\Models\DistrictResource as ResourceModel;

class DistrictResources
{
    public const RESOURCES_NAMES = [
        'metal',
        'graphite',
        'elerium',
        'paste',
        'science',
    ];

    public const SCIENCE_BASE_PRODUCTION = 1;
    public const SCIENCE_BASE_STORAGE = 0;

    public ResourceModel $metal;
    public ResourceModel $graphite;
    public ResourceModel $elerium;
    public ResourceModel $paste;
    public ResourceModel $science;

    public function __construct(int $district_id)
    {
        $resources = ResourceModel::query()
            ->select(['id', 'resource', 'last_quantity_updated', 'updated_at'])
            ->where('district_id', $district_id)
            ->get();

        if (5 !== $resources->count()) {
            abort(500, sprintf('district %s got %s resources instead of 4', $district_id, $resources->count()));
        }

        foreach ($this::RESOURCES_NAMES as $name) {
            $this->{$name} = $resources->where('resource', $name)->first();
            if (null === $this->{$name}) {
                abort(500, sprintf('district %s missing %s', $district_id, $name));
            }

            // Set base production and storage
            if ('science' == $name) {
                $this->{$name}->production = self::SCIENCE_BASE_PRODUCTION;
                $this->{$name}->max = self::SCIENCE_BASE_STORAGE;
            }
        }

        $this->setProductionAndQuantity($district_id);
    }

    /**
     * @uses ResourceModel::getQuantity()
     */
    public function toArray(): array
    {
        $resources = [];
        foreach ($this::RESOURCES_NAMES as $name) {
            $resources[$name] = [
                'max' => $this->{$name}->max,
                'production' => $this->{$name}->production,
                'value' => $this->{$name}->getQuantity(),
            ];
        }

        return $resources;
    }

    public static function getArray(int $district_id): array
    {
        $self = new self($district_id);

        return $self->toArray();
    }

    /**
     * Update the `max` and `production` property on ResourceModel.
     * @see ResourceModel::$max
     * @see ResourceModel::$production
     */
    private function setProductionAndQuantity(int $district_id): void
    {
        DistrictField::query()
            ->select(['x', 'y', 'value', 'level', 'district_id'])
            ->where('district_id', $district_id)
            ->whereIn('value', GameService::getCodeNames(array_merge(GameService::PRODUCTION_BUILDINGS, GameService::STORAGE_BUILDINGS)))
            ->get()
            ->each(function (DistrictField $field): void {
                /** @var AbstractStorageBuilding|AbstractProductionBuilding $building */
                $building = $field->getGameRule();
                if (method_exists($building, 'baseCapacity')) {
                    $this->{$building->getResourceName()}->max += $building->totalStorage();
                } else {
                    $naturalsCount = $field->getNaturalCount();
                    $this->{$building->getResourceName()}->production += $building->totalProduction($naturalsCount);
                }
            });
    }
}
