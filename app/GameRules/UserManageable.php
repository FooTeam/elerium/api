<?php

namespace App\GameRules;

use App\GameRules\Researches\EleriumAccelerator;
use App\Models\Research;
use App\Traits\HasCodeName;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

abstract class UserManageable
{
    use HasCodeName;

    abstract public function neededResearch(): ?string;

    /**
     * Check if the user have the required research for this research/building/unit.
     */
    public function isUnlocked(): bool
    {
        // Bail if no research is needed or on the first one (always unlocked)
        if (null === $this->neededResearch() || EleriumAccelerator::class === get_called_class()) {
            return true;
        }

        return Research::getPlayerResearches()
            ->where('name', $this->neededResearch()::codeName())->count() > 0;
    }

    /**
     * @throw HttpResponseException
     */
    public function validateUnlocked(): self
    {
        if ($this->isUnlocked()) {
            return $this;
        }

        throw new HttpResponseException(response()->json(['errors' => ['locked'], 'neededResearch' => $this->neededResearch()::codeName()], JsonResponse::HTTP_BAD_REQUEST));
    }
}
