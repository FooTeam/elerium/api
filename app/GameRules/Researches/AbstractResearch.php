<?php

namespace App\GameRules\Researches;

use App\GameRules\DistrictResources;
use App\GameRules\UserManageable;
use App\Models\District;
use App\Models\Research;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

abstract class AbstractResearch extends UserManageable
{
    /**
     * Return the science level of the research.
     */
    abstract public function neededScience(): int;

    /**
     * Return the researches unlocked by this research.
     * @see neededResearch for required research for this research
     * @return Research[]
     */
    abstract public function unlockedResearches(): array;

    public function toArray(): array
    {
        return [
            'name' => self::codeName(),
            'neededScience' => $this->neededScience(),
            'requiredElerium' => $this->neededElerium(),
        ];
    }

    public function createModel(): Research
    {
        return new Research(['name' => $this->codeName()]);
    }

    public static function getRuleFQDN(string $name): string
    {
        if (class_exists('App\\GameRules\\Researches\\Production\\'.$name)) {
            return 'App\\GameRules\\Researches\\Production\\'.$name;
        }
        if (class_exists('App\\GameRules\\Researches\\Storage\\'.$name)) {
            return 'App\\GameRules\\Researches\\Storage\\'.$name;
        }
        if (class_exists('App\\GameRules\\Researches\\Natural\\'.$name)) {
            return 'App\\GameRules\\Researches\\Natural\\'.$name;
        }
        return 'App\\GameRules\\Researches\\'.$name;
    }

    /**
     * Return the required elerium for the research.
     */
    abstract public function neededElerium(): int;

    public function consumeResources(District $district): void
    {
        $errors = [];
        $messages = [];

        $resources = DistrictResources::getArray($district->id);

        if ($resources['elerium']['value'] < $this->neededElerium()) {
            $errors[] = 'elerium';
            $messages[] = "{$this->neededElerium()} elerium needed, this district got {$resources['elerium']['value']}.";
        }

        if ($resources['science']['value'] < $this->neededScience()) {
            $errors[] = 'science';
            $messages[] = "{$this->neededScience()} science needed, this district got {$resources['science']['value']}.";
        }

        if ([] !== $errors) {
            throw new HttpResponseException(response()->json(['errors' => $errors, 'messages' => $messages], JsonResponse::HTTP_BAD_REQUEST));
        }

        $district->resources()->where('resource', 'elerium')->first()->consume($this->neededElerium());
        $district->resources()->where('resource', 'science')->first()->consume($this->neededScience());
    }
}
