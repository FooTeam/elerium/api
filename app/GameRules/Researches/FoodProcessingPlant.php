<?php

namespace App\GameRules\Researches;

use App\GameRules\Researches\Natural\SoilEnrichment;

class FoodProcessingPlant extends AbstractResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [];
    }

    public function neededResearch(): ?string
    {
        return SoilEnrichment::class;
    }
}
