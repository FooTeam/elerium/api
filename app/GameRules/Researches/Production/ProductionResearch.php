<?php

namespace App\GameRules\Researches\Production;

use App\GameRules\Researches\AbstractResearch;

abstract class ProductionResearch extends AbstractResearch
{
    /**
     * Return the resources targeted by the research.
     */
    abstract public function resources(): array;

    /**
     * Return the boost apply to the resource's production.
     */
    abstract public function boost(): float;
}
