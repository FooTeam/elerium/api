<?php

namespace App\GameRules\Researches\Production;

use App\GameRules\Researches\Natural\GroundScanner;

class DeepDrilling extends ProductionResearch
{
    public function neededScience(): int
    {
        return 500;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [];
    }

    public function neededResearch(): ?string
    {
        return GroundScanner::class;
    }

    public function resources(): array
    {
        return ['metal', 'graphite'];
    }

    public function boost(): float
    {
        return 1;
    }
}
