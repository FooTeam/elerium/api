<?php

namespace App\GameRules\Researches\Natural;

use App\GameRules\Naturals\FertileSoil;
use App\GameRules\Researches\FoodProcessingPlant;
use App\GameRules\Researches\Storage;

class SoilEnrichment extends NaturalResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [FoodProcessingPlant::class];
    }

    public function neededResearch(): ?string
    {
        return Storage::class;
    }

    public function natural(): array
    {
        return [FertileSoil::codeName()];
    }

    public function boost(): float
    {
        return 1;
    }
}
