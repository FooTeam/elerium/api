<?php

namespace App\GameRules\Researches\Natural;

use App\GameRules\Naturals\MeteoriteImpact;

class HighSpeedParticulesCompressor extends NaturalResearch
{
    public function neededScience(): int
    {
        return 500;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [];
    }

    public function neededResearch(): ?string
    {
        return null;
    }

    public function natural(): array
    {
        return [MeteoriteImpact::codeName()];
    }

    public function boost(): float
    {
        return 1;
    }
}
