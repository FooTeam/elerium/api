<?php

namespace App\GameRules\Researches\Natural;

use App\GameRules\Researches\AbstractResearch;

abstract class NaturalResearch extends AbstractResearch
{
    /**
     * Return the natural targeted by the research.
     */
    abstract public function natural(): array;

    /**
     * Return the boost apply to the resource's production.
     */
    abstract public function boost(): float;
}
