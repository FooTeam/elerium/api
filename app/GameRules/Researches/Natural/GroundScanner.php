<?php

namespace App\GameRules\Researches\Natural;

use App\GameRules\Naturals\GraphiteDeposit;
use App\GameRules\Naturals\MetalDeposit;
use App\GameRules\Researches\Production\DeepDrilling;
use App\GameRules\Researches\Storage;

class GroundScanner extends NaturalResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [DeepDrilling::class];
    }

    public function neededResearch(): ?string
    {
        return Storage::class;
    }

    public function natural(): array
    {
        return [MetalDeposit::codeName(), GraphiteDeposit::codeName()];
    }

    public function boost(): float
    {
        return 1;
    }
}
