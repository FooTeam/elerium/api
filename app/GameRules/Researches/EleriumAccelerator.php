<?php

namespace App\GameRules\Researches;

class EleriumAccelerator extends AbstractResearch
{
    public function neededScience(): int
    {
        return 500;
    }

    public function neededElerium(): int
    {
        return 100;
    }

    public function unlockedResearches(): array
    {
        return [AdvancedBuildingsResearch::class];
    }

    public function neededResearch(): ?string
    {
        return null;
    }
}
