<?php

namespace App\GameRules\Researches;

class Starshipyard extends AbstractResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [];
    }

    public function neededResearch(): ?string
    {
        return AdvancedBuildingsResearch::class;
    }
}
