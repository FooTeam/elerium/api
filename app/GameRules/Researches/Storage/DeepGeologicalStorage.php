<?php

namespace App\GameRules\Researches\Storage;

class DeepGeologicalStorage extends StorageResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [];
    }

    public function neededResearch(): ?string
    {
        return StackingAlgorithm::class;
    }

    public function boost(): float
    {
        return 1;
    }
}
