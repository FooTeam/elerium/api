<?php

namespace App\GameRules\Researches\Storage;

use App\GameRules\Researches\Natural\HighSpeedParticulesCompressor;
use App\GameRules\Researches\Storage;

class StackingAlgorithm extends StorageResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [DeepGeologicalStorage::class, HighSpeedParticulesCompressor::class];
    }

    public function neededResearch(): ?string
    {
        return Storage::class;
    }

    public function boost(): float
    {
        return 0.5;
    }
}
