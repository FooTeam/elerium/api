<?php

namespace App\GameRules\Researches\Storage;

use App\GameRules\Researches\AbstractResearch;

abstract class StorageResearch extends AbstractResearch
{
    /**
     * Return the boost apply to the resource's production.
     */
    abstract public function boost(): float;
}
