<?php

namespace App\GameRules\Researches;

class AdvancedBuildingsResearch extends AbstractResearch
{
    public function neededScience(): int
    {
        return 10;
    }

    public function neededElerium(): int
    {
        return 100;
    }

    public function unlockedResearches(): array
    {
        return [Storage::class, Starshipyard::class];
    }

    public function neededResearch(): ?string
    {
        return EleriumAccelerator::class;
    }
}
