<?php

namespace App\GameRules\Researches;

use App\GameRules\Researches\Natural\GroundScanner;
use App\GameRules\Researches\Natural\SoilEnrichment;
use App\GameRules\Researches\Storage\StackingAlgorithm;

class Storage extends AbstractResearch
{
    public function neededScience(): int
    {
        return 0;
    }

    public function neededElerium(): int
    {
        return 0;
    }

    public function unlockedResearches(): array
    {
        return [SoilEnrichment::class, GroundScanner::class, StackingAlgorithm::class];
    }

    public function neededResearch(): ?string
    {
        return AdvancedBuildingsResearch::class;
    }
}
