<?php

namespace App\GameRules;

use App\GameRules\Naturals\FertileSoil;
use App\GameRules\Naturals\GraphiteDeposit;
use App\GameRules\Naturals\MetalDeposit;
use App\GameRules\Naturals\MeteoriteImpact;
use App\Models\District as DistrictModel;
use App\Models\DistrictField;
use Exception;
use Illuminate\Support\Collection;

class District
{
    private const NATURALS = [
        FertileSoil::class,
        GraphiteDeposit::class,
        MetalDeposit::class,
        MeteoriteImpact::class,
    ];

    private const MAX_NEIGHBORS = 6; // Since we're working with hexagons

    private const MALUS = 0.1; // 10%

    /** @var Natural[] */
    private static $pool = [];

    /**
     * Generate the Natural deposits.
     * @return Collection<DistrictField>
     */
    public static function getFieldsWithRandomNatural(): Collection
    {
        $grid = [];

        self::preparePool();
        self::prepareDistrict($grid);
        self::generateNaturalOrigins($grid);
        self::generateNaturalVeins($grid);

        return collect($grid)
            ->flatten()
            ->filter(fn ($field) => is_a($field, DistrictField::class));
    }

    /**
     * Randomly place Natural origins depending their probability.
     * @param array $grid
     * @return void
     */
    private static function generateNaturalOrigins(array &$grid): void
    {
        while (count(self::$pool) > 0) {
            $x = random_int(0, DistrictModel::WIDTH);
            $y = random_int(0, DistrictModel::HEIGHT);

            if (null === $grid[$y][$x]) {
                $natural = self::getRandomNatural();
                if (null !== $natural) {
                    $grid[$y][$x] = [
                        'field' => new DistrictField([
                            'x' => $x,
                            'y' => $y,
                            'value' => $natural::codeName(),
                        ]),
                        'natural' => $natural,
                    ];
                }
            }
        }
    }

    /**
     * Generate a Natural vein from its origin.
     * @param array $grid
     * @return void
     */
    private static function generateNaturalVeins(array &$grid): void
    {
        $origins = collect($grid)
            ->flatten()
            ->filter(fn ($field) => is_a($field, DistrictField::class));

        foreach ($origins as $origin) {
            self::makeVein(
                $grid,
                $grid[$origin->y][$origin->x]['field'],
                $grid[$origin->y][$origin->x]['natural']
            );
        }
    }

    /**
     * A recursive method to create a Natural vein.
     * @param array $grid
     * @param DistrictField $field
     * @param string $natural The Natural class FQDN
     * @param int $depth The current depth of the vein
     * @return void
     * @throws Exception https://www.php.net/manual/en/function.random-int.php#refsect1-function.random-int-errors
     */
    private static function makeVein(array &$grid, DistrictField $field, string $natural, int $depth = 0): void
    {
        if (array_key_exists('next', $natural::getProbabilities())) {
            // Get fields' neighbors within the grid boundaries
            $neighbors = $field->getDirections()
                ->map(fn ($direction) => $field->add($direction))
                ->filter(
                    fn ($field) => $field->isInBounds() && null === $grid[$field->y][$field->x]
                );

            $probability = $natural::getProbabilities()['next'];
            // Bonus based on the possible amount of neighbors
            $probability *= 1 + (self::MAX_NEIGHBORS - count($neighbors)) / ((self::MAX_NEIGHBORS + count($neighbors)) / 2);
            // Malus based on the current depth of the vein
            $probability -= $natural::getProbabilities()['next'] * self::MALUS * $depth;

            foreach ($neighbors as $neighbor) {
                $percentage = random_int(1, 100);

                if ($percentage <= $probability) {
                    $grid[$neighbor['y']][$neighbor['x']] = [
                        'field' => new DistrictField([
                            'x' => $neighbor['x'],
                            'y' => $neighbor['y'],
                            'value' => $natural::codeName(),
                        ]),
                        'natural' => $natural,
                    ];

                    self::makeVein(
                        $grid,
                        $grid[$neighbor['y']][$neighbor['x']]['field'],
                        $natural,
                        ++$depth
                    );
                }
            }
        }
    }

    /**
     * Randomly draw a Natural from the pool without replacement.
     * @return string|null The Natural class FQDN or null
     */
    private static function getRandomNatural(): ?string
    {
        $index = random_int(0, count(self::$pool) - 1);
        $natural = self::$pool[$index];

        $percentage = random_int(1, 100);
        if ($percentage <= $natural::getProbabilities()['probability']) {
            array_splice(self::$pool, $index, 1);

            return $natural;
        }

        return null;
    }

    /**
     * Create a shuffled pool containing a number of Naturals based on their frequency.
     * @return void
     */
    private static function preparePool(): void
    {
        foreach (self::NATURALS as $natural) {
            self::$pool = array_merge(self::$pool, array_fill(0, $natural::getProbabilities()['frequency'], $natural));
        }
        shuffle(self::$pool);
    }

    /**
     * Create a HEIGHTxWIDTH empty grid.
     * @param array $grid
     * @return void
     */
    private static function prepareDistrict(array &$grid): void
    {
        $grid = array_fill(0, DistrictModel::HEIGHT + 1, array_fill(0, DistrictModel::WIDTH + 1, null));
    }
}
