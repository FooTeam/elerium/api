<?php

namespace App\GameRules;

use App\Models\District;
use App\Models\Stargate;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class Planet
{
    /**
     * The size of a planet (represented by a square).
     */
    public const SIZE = 33;

    /**
     * Check if given coordinates corresponds to an occupied cell.
     */
    public static function isOccupied(int $x, int $y): bool
    {
        return
            District::query()
                ->where('x', $x)
                ->where('y', $y)
                ->exists()
            || Stargate::query()
                ->where('x', $x)
                ->where('y', $y)
                ->exists();
    }

    public static function getFields(): Collection
    {
        $fields = new Collection;
        $fields = $fields->concat(District::query()->select(['id', 'x', 'y', 'name', 'user_id'])->get());

        return $fields->concat(Stargate::query()->select(['id', 'x', 'y', 'color', 'user_id'])->get());
    }

    public static function getDistanceBetweenFields(int $px, int $py, int $qx, int $qy): int
    {
        return Cache::rememberForever("planet_distance_{$px}-{$py}--{$qx}-{$qy}", function () use ($px, $py, $qx, $qy) {
            $distances = [];

            for ($x = 0; $x < 3; $x++) {
                for ($y = 0; $y < 3; $y++) {
                    $distances[$x.$y] = sqrt(
                        (($qx + $x * self::SIZE) - $px) ** 2
                        + (($qy + $y * self::SIZE) - $py) ** 2
                    );

                    // Bail if a distance found is 1
                    if (1 === $distances[$x.$y]) {
                        return 1;
                    }
                }
            }

            return min($distances);
        });
    }
}
