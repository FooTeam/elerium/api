<?php

namespace App\Traits;

/**
 * Inspired by LaravelTreats\Model\Traits\HasCompositePrimaryKey.
 * @licence MIT
 */
trait HasCompositePrimaryKey
{
    /**
     * {@inheritdoc}
     */
    protected function getKeyForSaveQuery()
    {
        $primaryKeyForSaveQuery = [count($this->primaryKey)];

        foreach ($this->primaryKey as $i => $pKey) {
            $primaryKeyForSaveQuery[$i] = isset($this->original[$this->getKeyName()[$i]])
                ? $this->original[$this->getKeyName()[$i]]
                : $this->getAttribute($this->getKeyName()[$i]);
        }

        return $primaryKeyForSaveQuery;
    }

    /**
     * {@inheritdoc}
     */
    protected function setKeysForSaveQuery($query)
    {
        foreach ($this->primaryKey as $i => $pKey) {
            $query->where($this->getKeyName()[$i], '=', $this->getKeyForSaveQuery()[$i]);
        }

        return $query;
    }

    private static function findQuery(array $columns)
    {
        $query = self::query();

        foreach ($columns as $key => $value) {
            $query->where($key, '=', $value);
        }

        return $query;
    }
}
