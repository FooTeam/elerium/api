<?php

namespace App\Traits;

use App\Models\District;
use Illuminate\Database\Eloquent\Collection;

trait HasNeighbors
{
    public static $evenDirections = [
        ['x' => 1, 'y' => 0],
        ['x' => 0, 'y' => 1],
        ['x' => -1, 'y' => 1],
        ['x' => -1, 'y' => 0],
        ['x' => -1, 'y' => -1],
        ['x' => 0, 'y' => -1],
    ];

    public static $oddDirections = [
        ['x' => 1, 'y' => 0],
        ['x' => 1, 'y' => 1],
        ['x' => 0, 'y' => 1],
        ['x' => -1, 'y' => 0],
        ['x' => 0, 'y' => -1],
        ['x' => 1, 'y' => -1],
    ];

    public function getDirections(): \Illuminate\Support\Collection
    {
        return collect($this->y % 2 === 0 ? self::$evenDirections : self::$oddDirections);
    }

    public function getNeighbors(string $related = null): Collection
    {
        /** @var $query \Illuminate\Database\Eloquent\Builder */
        $query = $this->buildNeighborsQuery();
        if (null === $related) {
            return $query->get();
        }

        return $query->where('value', $related)->get();
    }

    public function add(array $direction): static
    {
        return new static([
            'x' => $this->x + $direction['x'],
            'y' => $this->y + $direction['y'],
        ]);
    }

    public function isInBounds()
    {
        return $this->x >= 0 && $this->x <= District::WIDTH
            && $this->y >= 0 && $this->y <= District::HEIGHT;
    }

    private function buildNeighborsQuery()
    {
        $query = static::where('district_id', $this->district_id);

        return $query->where(
            fn ($query) => $this->getDirections()
                ->map(fn ($direction) => $this->add($direction))
                ->each(
                    fn ($field) => $query->orWhere(
                        fn ($query) => $query
                            ->where('x', $field->x)
                            ->where('y', $field->y)
                    )
                )
        );
    }
}
