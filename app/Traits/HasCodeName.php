<?php

namespace App\Traits;

trait HasCodeName
{
    /**
     * Return the code-friendly name of the rule.
     */
    public static function codeName(): string
    {
        return (new \ReflectionClass(get_called_class()))->getShortName();
    }
}
