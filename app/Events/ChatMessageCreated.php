<?php

namespace App\Events;

use Duijker\LaravelMercureBroadcaster\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatMessageCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $player;
    public string $channel;
    public string $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $player, string $channel, string $message)
    {
        $this->player = $player;
        $this->channel = $channel;
        $this->message = $message;
    }

    public function broadcastOn()
    {
        return new Channel(
            env('CLIENT_URL', 'https://elerium.footeam.fr/').'chat/'.$this->channel,
            true
        );
    }
}
