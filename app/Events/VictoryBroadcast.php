<?php

namespace App\Events;

use Duijker\LaravelMercureBroadcaster\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VictoryBroadcast implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $faction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $faction)
    {
        $this->faction = $faction;
    }

    public function broadcastOn()
    {
        return new Channel(
            env('CLIENT_URL', 'https://elerium.footeam.fr/').'victory',
            true
        );
    }
}
