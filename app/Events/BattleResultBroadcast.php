<?php

namespace App\Events;

use Duijker\LaravelMercureBroadcaster\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BattleResultBroadcast implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public int $from;
    public int $to;
    public bool $win;
    public string $username;
    public string $friend;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $from, int $to, bool $win, bool $friend, string $username)
    {
        $this->from = $from;
        $this->to = $to;
        $this->win = $win;
        $this->username = $username;
        $this->friend = $friend;
    }

    public function broadcastOn()
    {
        return new Channel(
            env('CLIENT_URL', 'https://elerium.footeam.fr/').'battle/'.$this->username,
            true
        );
    }
}
