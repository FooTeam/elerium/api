<?php

namespace App\Console\Commands;

use App\Models\Stargate;
use Illuminate\Console\Command;

class evaluateFleets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evaluate:fleets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Evaluate if a fleet is arrived to a stargate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Stargate::query()
            ->get()
            ->each(fn (Stargate $stargate) => $stargate->evaluateFleets());
    }
}
