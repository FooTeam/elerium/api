<?php

namespace App\Console\Commands;

use App\Models\Stargate;
use Illuminate\Console\Command;

class InitiateGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:game';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initiate a game';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        if (Stargate::addStargatesToPlanet()) {
            $this->info('Stargates added to planet.');

            return;
        }

        $this->error('There are already stargates in the database');
    }
}
