<?php

namespace App\Console\Commands;

use App\Models\Faction;
use Illuminate\Console\Command;

class evaluateFactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evaluate:factions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Evaluate if factions gain point with stargates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Faction::query()->get()->each(function (Faction $faction) {
            $faction->evaluatePoint();
        });
    }
}
