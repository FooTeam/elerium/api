<?php

namespace App\Models;

use App\Events\BattleResultBroadcast;
use App\GameRules\Planet;
use App\GameRules\Units\AbstractUnit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $x
 * @property int $y
 * @property string $color
 * @property int $user_id
 * @property User $user
 * @property \Illuminate\Support\Carbon $updated_at
 * @see \CreateStargatesTable
 */
class Stargate extends Model
{
    use HasFactory;

    // Disable the created_at field
    public const CREATED_AT = null;

    public const COLORS = ['yellow', 'orange', 'violet', 'red'];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fleets(): HasMany
    {
        return $this->hasMany(Fleet::class);
    }

    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    }

    public function capture(int $user_id): void
    {
        $this->user_id = $user_id;
        $this->save();
    }


    public static function addStargatesToPlanet(): bool
    {
        if (self::query()->count('id')) {
            return false;
        }

        foreach (array_merge(self::COLORS, self::COLORS) as $color) {
            do {
                $x = random_int(0, Planet::SIZE);
                $y = random_int(0, Planet::SIZE);
            } while (Planet::isOccupied($x, $y));

            self::factory()->create(['color' => $color, 'x' => $x, 'y' => $y]);
        }

        return true;
    }

    public function looseUnit(AbstractUnit $unit): void
    {
        $function = new \ReflectionClass($unit::class);

        $units = $this->units()->where('type', $function->getShortName())->first();
        $units->quantity--;
        if ($units->quantity < 1) {
            $units->delete();
            return;
        }
        $units->save();
    }

    public function evaluateFleets(): void
    {
        // Get all fleet wich are arrived
        $fleetArrived = $this->fleets()->get()->reduce(function (array $arrived, Fleet $fleet) {
            $minutesPast = $fleet->created_at->diffInRealMinutes();
            if ($minutesPast >= $fleet->getTravelTime()) {
                array_push($arrived, $fleet);
            }
            return $arrived;
        }, []);
        
        // Sort arrived fleet to evaluate the first arrived before the second
        usort($fleetArrived, function (Fleet $a, Fleet $b): int {
            $arrived_a = $a->created_at->diffInRealMinutes() - $a->getTravelTime();
            $arrived_b = $b->created_at->diffInRealMinutes() - $b->getTravelTime();

            if ($arrived_a > $arrived_b) {
                return 1;
            }
            if ($arrived_a < $arrived_b) {
                return -1;
            }
            return 0;
        });

        foreach ($fleetArrived as $fleet) {
            /** @var Fleet $fleet */
            // If it's not the faction wich control the stargate, it's the bagarre
            if ($this->user != null && $fleet->from->user->faction != $this->user->faction) {
                $win = $fleet->attack();
                if ($win) {
                    $fleet->capture();
                }
                BattleResultBroadcast::dispatch(
                    $fleet->from->id,
                    $fleet->to->id,
                    $fleet->units()->count() > 0,
                    false,
                    $fleet->user()->first()->username
                );
            } else {
                $summary = new BattleSummary([
                    'stargate_id' => $fleet->to->id,
                    'district_id' => $fleet->from->id,
                ]);
                $summary->save();
                $fleet->capture();
                BattleResultBroadcast::dispatch(
                    $fleet->from->id,
                    $fleet->to->id,
                    true,
                    true,
                    $fleet->user()->first()->username
                );
            }

            $fleet->delete();
            $this->refresh();
        }
    }
}
