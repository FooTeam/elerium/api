<?php

namespace App\Models;

use App\GameRules\Buildings\DistrictCouncil;
use App\GameRules\Buildings\Starshipyard;
use App\GameRules\District as DistrictRules;
use App\GameRules\DistrictResources;
use App\GameRules\Planet;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property string $name
 * @property User $user
 * @property int $x
 * @property int $y
 * @property DistrictField[] $fields
 * @property Units[] $units
 * @property \Illuminate\Support\Carbon $created_at
 */
class District extends Model
{
    use HasFactory;

    // Max width & height (starts at 0, WIDTH = 14 means a width of 15)
    public const WIDTH = 14;
    public const HEIGHT = 10;

    // Disable the updated_at field
    public const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'x', 'y',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fields(): HasMany
    {
        return $this->hasMany(DistrictField::class);
    }

    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    }

    public function fleets(): HasMany
    {
        return $this->hasMany(Fleet::class);
    }

    public function battleSummaries(): HasMany
    {
        return $this->hasMany(BattleSummary::class);
    }

    public function field(int $x, int $y)
    {
        return $this->fields()
            ->where('x', '=', $x)
            ->where('y', '=', $y)
            ->first();
    }

    public function resources(): HasMany
    {
        return $this->hasMany(DistrictResource::class);
    }

    public function level(): int
    {
        return $this->fields()
            ->select('level')
            ->where('value', '=', DistrictCouncil::codeName())
            ->first()->level;
    }

    public function getUnitStorage(): int
    {
        return $this->fields()
            ->where('value', Starshipyard::codeName())
            ->select(['level', 'value'])
            ->get()
            ->reduce(fn (int $max, DistrictField $starshipyard) => $max + $starshipyard->getGameRule()->unitStorage(), 0);
    }

    public static function create(User $owner): self
    {
        do {
            $x = random_int(0, Planet::SIZE);
            $y = random_int(0, Planet::SIZE);
        } while (Planet::isOccupied($x, $y));

        $district = new self(['x' => $x, 'y' => $y]);

        $predefinedNames = include resource_path('cityNames.php');
        $district->name = $predefinedNames[$owner->id % count($predefinedNames)];

        $owner->districts()->save($district);

        $district->fields()->saveMany(DistrictRules::getFieldsWithRandomNatural());
        $district->fields()->updateOrCreate([
            'x' => (int) (self::WIDTH / 2),
            'y' => (int) (self::HEIGHT / 2),
        ], [
            'value' => DistrictCouncil::codeName(),
            'level' => 1,
        ]);

        $resources = array_map(fn ($name) => new DistrictResource(['resource' => $name]), DistrictResources::RESOURCES_NAMES);

        $district->resources()->saveMany($resources);

        return $district;
    }
}
