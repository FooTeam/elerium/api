<?php

namespace App\Models;

use App\Events\BattleResultBroadcast;
use App\GameRules\Planet;
use App\GameRules\Units\AbstractUnit;
use App\GameRules\Units\UnitFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * @property District $from
 * @property Stargate $to
 * @property \Illuminate\Support\Carbon $created_at
 */
class Fleet extends Model
{
    use HasFactory;

    // Disable the updated_at field
    public const UPDATED_AT = null;

    private BattleSummary $summary;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'district_id', 'stargate_id',
    ];

    public function from(): BelongsTo
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function to(): BelongsTo
    {
        return $this->belongsTo(Stargate::class, 'stargate_id');
    }

    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    }

    public function getSpeed(): int
    {
        return Cache::rememberForever("fleet_{$this->id}_speed", function () {
            return $this->units()
                ->select('type')
                ->get()
                ->reduce(
                    fn (int $speed, Unit $unit) => min($speed, $unit->getGameRule()->getTravelSpeed()),
                    PHP_INT_MAX
                );
        });
    }

    /**
     * @return float|int time in minutes
     */
    public function getTravelTime(): float | int
    {
        return (
                Planet::getDistanceBetweenFields($this->from->x, $this->from->y, $this->to->x, $this->to->y)
                / $this->getSpeed()
            ) * 60;
    }

    /**
     * @return bool true if the fleet win, false otherwise
     */
    public function attack(): bool
    {
        DB::beginTransaction();

        $this->summary = new BattleSummary([
            'stargate_id' => $this->to->id,
            'district_id' => $this->from->id,
        ]);
        $this->summary->save();

        $stargateUnits = $this->to->units()->get()->reduce(function ($units, Unit $unit) {
            for ($i=0; $i<$unit->quantity; $i++) {
                array_push($units, UnitFactory::create($unit->type));
            }
            return $units;
        }, []);

        if (sizeof($stargateUnits) == 0) {
            $this->summary->win = true;
            $this->summary->save();
            DB::commit();
            return true;
        }
        shuffle($stargateUnits);

        $fleetUnits = $this->units()->get()->reduce(function ($units, Unit $unit) {
            for ($i=0; $i<$unit->quantity; $i++) {
                array_push($units, UnitFactory::create($unit->type));
            }
            return $units;
        }, []);
        shuffle($fleetUnits);
        
        foreach ($fleetUnits as $fleetKey => $fleetUnit) {
            foreach ($stargateUnits as $stargateKey => $stargateUnit) {
                while ($fleetUnit->getDefense() > 0 && $stargateUnit->getDefense() > 0) {
                    $fleetUnit->defense -= $stargateUnit->getAttack();
                    $stargateUnit->defense -= $fleetUnit->getAttack();
                }
                if ($stargateUnit->getDefense() < 1) {
                    $this->to->looseUnit($stargateUnit);
                    array_splice($stargateUnits, $stargateKey, 1);
                }
                if ($fleetUnit->getDefense() < 1) {
                    $this->looseUnit($fleetUnit);
                    array_splice($fleetUnits, $fleetKey, 1);
                    break;
                }
            }
        }

        $this->summary->win = $this->units()->count() > 0;
        $this->summary->save();

        DB::commit();
        return $this->summary->win;
    }

    public function looseUnit(AbstractUnit $unit): void
    {
        $function = new \ReflectionClass($unit::class);
        $units = $this->units()->where('type', $function->getShortName())->first();
        $units->quantity--;
        if ($units->quantity < 1) {
            $units->delete();
            return;
        }
        $this->summary->addUnit($unit);
        $units->save();
    }

    public function user(): User
    {
        return $this->from->user()->first();
    }

    public function capture(): void
    {
        // calcul point for old faction
        if ($this->to->user != null) {
            $faction = $this->to->user->faction()->first();
            $faction->{$this->to->color} += $this->to->updated_at->diffInRealMinutes();
            $faction->save();
        }
        // conquiere
        $this->to->user_id = $this->user()->id;
        $this->to->save();
        $this->units()->each(function (Unit $unit): void {
            $unit->fleet_id = null;
            $unit->stargate_id = $this->to->id;
            $unit->save();
        });
    }
}
