<?php

namespace App\Models;

use App\Events\VictoryBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $name
 * @property int $yellow
 * @property int $orange
 * @property int $violet
 * @property int $red
 * @property \Illuminate\Support\Carbon $updated_at
 * @property Collection<User> $members
 */
class Faction extends Model
{
    // Disable the created_at field
    public const CREATED_AT = null;
    protected $primaryKey = 'name';
    protected $keyType = 'string';

    public const NAMES = ['federation', 'empire', 'conglomerate'];
    public const VICTORY = 20000;

    public function members(): HasMany
    {
        return $this->hasMany(User::class, 'faction', 'name');
    }

    public function evaluatePoint(): void
    {
        Stargate::query()
            ->get()
            ->each(function (Stargate $stargate) {
                if ($stargate->user()->first() != null && $stargate->user()->first()->faction == $this->name) {
                    $nb = $this->updated_at->diffInRealMinutes();
                    $this->{$stargate->color} += $nb;
                    if ($this->{$stargate->color} > self::VICTORY) {
                        $this->{$stargate->color} = self::VICTORY;
                    }
                }
            });
        $this->save();

        if ($this->win()) {
            VictoryBroadcast::dispatch($this->name);
        }
    }

    public function win(): bool
    {
        return $this->yellow >= self::VICTORY
            && $this->orange >= self::VICTORY
            && $this->violet >= self::VICTORY
            && $this->red >= self::VICTORY;
    }
}
