<?php

namespace App\Models;

use App\GameRules\Units\AbstractUnit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $type
 * @property number $quantity
 * @property District $district
 * @property Stargate $stargate
 * @property Fleet $fleet
 */
class Unit extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'quantity', 'district_id', 'fleet_id', 'stargate_id', 'battle_summary_id',
    ];

    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function stargate(): BelongsTo
    {
        return $this->belongsTo(Stargate::class);
    }

    public function summary(): BelongsTo
    {
        return $this->belongsTo(BattleSummary::class);
    }

    public function fleet(): BelongsTo
    {
        return $this->belongsTo(Fleet::class);
    }

    public function getGameRule(): AbstractUnit
    {
        return new ('App\\GameRules\\Units\\'.$this->type);
    }
}
