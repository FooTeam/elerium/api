<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property string $resource
 * @property int $last_quantity_updated
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $district_id
 * @property District $district
 */
class DistrictResource extends Model
{
    use HasFactory;

    // Disable the created_at field
    public const CREATED_AT = null;

    /** @see DistrictResources::setProductionAndQuantity() */
    public int $production = 100;
    public int $max = 1000;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'resource',
        'last_quantity_updated',
    ];

    public function district(): HasOne
    {
        return $this->hasOne(District::class);
    }

    /**
     * @used-by DistrictResources::toArray()
     */
    public function getQuantity(): int
    {
        $this->last_quantity_updated += round($this->production / 60 * $this->updated_at->diffInRealSeconds(), 0, PHP_ROUND_HALF_DOWN);

        if (0 != $this->max && $this->last_quantity_updated > $this->max) {
            $this->last_quantity_updated = $this->max;
        }

        $this->save();

        return $this->last_quantity_updated;
    }

    public function consume(int $consumed): void
    {
        $this->last_quantity_updated -= $consumed;
        $this->save();
    }
}
