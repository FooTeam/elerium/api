<?php

namespace App\Models;

use App\GameRules\Units\AbstractUnit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BattleSummary extends Model
{
    use HasFactory;
    
    // Disable the updated_at field
    public const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'win', 'district_id', 'stargate_id',
    ];

    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function location(): BelongsTo
    {
        return $this->belongsTo(Stargate::class, 'stargate_id');
    }

    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    }

    public function addUnit(AbstractUnit $unit): void
    {
        $function = new \ReflectionClass($unit::class);
        $units = $this->units()->where('type', $function->getShortName())->first();

        if ($units == null) {
            $units = new Unit([
                'type' => $function->getShortName(),
                'battle_summary_id' => $this->id,
                'quantity' => 0,
            ]);
        }

        $units->quantity++;
        $units->save();
    }
}
