<?php

namespace App\Models;

use App\GameRules\Buildings\AbstractProductionBuilding;
use App\GameRules\UserManageable;
use App\Traits\HasCompositePrimaryKey;
use App\Traits\HasNeighbors;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $x
 * @property int $y
 * @property string $value
 * @property ?int $level
 * @property District $district
 */
class DistrictField extends Model
{
    use HasFactory, HasCompositePrimaryKey, HasNeighbors;

    // Disable the timestamps fields
    public $timestamps = false;
    // Disable the id default primary key
    public $incrementing = false;
    // Use theses fields as a composite primary key
    protected $primaryKey = ['x', 'y', 'district_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'x',
        'y',
        'value',
        'level',
    ];

    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function getGameRule(): UserManageable
    {
        $className = 'App\\GameRules\\';

        if (null === $this->level) {
            $className .= 'Naturals\\'.$this->value;

            return new $className;
        }

        $className .= 'Buildings\\'.$this->value;

        return new $className($this->level);
    }

    public function getNaturalCount(): int
    {
        $building = $this->getGameRule();

        if (! is_subclass_of($building, AbstractProductionBuilding::class)) {
            return 0;
        }
        if (null === $related = $building->getRelatedNatural()) {
            return 0;
        }

        return $this->getNeighbors($related)->count();
    }

    public function upgrade(): self
    {
        $this->level++;
        $this->save();

        return $this;
    }
}
