<?php

namespace App\Models;

use App\GameRules\Researches\AbstractResearch;
use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * @property string $name
 * @property User $user
 */
class Research extends Model
{
    use HasFactory, HasCompositePrimaryKey;

    // Disable the timestamps fields
    public $timestamps = false;
    // Disable the id default primary key
    public $incrementing = false;
    // Use theses fields as a composite primary key
    protected $primaryKey = ['user_id', 'name'];
    // Specify the plural of research
    protected $table = 'researches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }

    public function getGameRule(): AbstractResearch
    {
        $className = AbstractResearch::getRuleFQDN($this->name);

        return new $className;
    }

    /**
     * @return Collection<Research>
     */
    public static function getPlayerResearches(): Collection
    {
        /** @var User $user */
        $user = Auth::user();

        return Cache::rememberForever('user_researches_'.$user->id, fn () => $user->researches()->get());
    }

    public static function forgetCache(): void
    {
        /** @var User $user */
        $user = Auth::user();
        Cache::forget('user_researches_'.$user->id);
    }
}
