<?php

namespace App\Http\Controllers;

use App\GameRules\Buildings\Starshipyard;
use App\GameRules\DistrictResources;
use App\GameRules\Fleet as GameRulesFleet;
use App\GameRules\GameService;
use App\GameRules\Units\AbstractUnit;
use App\Http\Requests\BuildingSelection;
use App\Http\Requests\NewBuilding;
use App\Http\Requests\NewUnit;
use App\Http\Requests\SelectDistrictField;
use App\Http\Requests\UnitsSelection;
use App\Http\Resources\BattleSummaries as BattleSummariesResource;
use App\Http\Resources\Building;
use App\Http\Resources\District as ApiDistrictResource;
use App\Http\Resources\DistrictField as FieldResource;
use App\Http\Resources\Fleet as ApiFleetResource;
use App\Http\Resources\Unit as ApiUnitResource;
use App\Models\District;
use App\Models\DistrictField;
use App\Models\Fleet;
use App\Models\Stargate;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class DistrictController extends Controller
{
    public function getOwnDistricts(): AnonymousResourceCollection
    {
        /** @var User $user */
        $user = Auth::user();

        return ApiDistrictResource::collection($user->districts()->with(['fields'])->get());
    }

    public function addNewBuilding(int $id, NewBuilding $request): FieldResource
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        if ($district->fields()
            ->where('x', '=', $request->input('x'))
            ->where('y', '=', $request->input('y'))
            ->exists()) {
            throw new HttpResponseException(response()->json(['errors' => ['taken'], 'messages' => ['This field is taken']], JsonResponse::HTTP_BAD_REQUEST));
        }

        $building = new DistrictField(array_merge($request->validated(), ['level' => 0]));
        $building->getGameRule()->validateUnlocked()->validateUpgradeAndConsumeResources($district);
        $building->level = 1;

        $district->fields()->save($building);

        return new FieldResource($building);
    }

    public function upgradeBuilding(int $id, SelectDistrictField $request): FieldResource
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        /** @var DistrictField $building */
        $building = $district->fields()
            ->where('x', '=', $request->input('x'))
            ->where('y', '=', $request->input('y'))
            ->where('level', '!=', null)
            ->firstOrFail();

        $building->getGameRule()->validateUpgradeAndConsumeResources($district);

        return new FieldResource($building->upgrade());
    }

    public function demolish(int $id, SelectDistrictField $request)
    {
        $coordinates = $request->validated();

        if ($coordinates['x'] == District::WIDTH / 2 && $coordinates['y'] == District::HEIGHT / 2) {
            abort(Response::HTTP_BAD_REQUEST, "You can't demolish the district council");
        }

        $deleted = DistrictField::query()
            ->where('district_id', $id)
            ->where('x', $request->input('x'))
            ->where('y', $request->input('y'))
            ->delete();

        if (0 === $deleted) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return response()->noContent();
    }

    public function addNewUnit(int $id, NewUnit $request): ApiUnitResource
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        // TODO: cache unit count (+ clear cache at unit creation and fleet assignation)
        $unitCount = $district->units()->sum('quantity');

        if ($district->getUnitStorage() < $unitCount + $request->input('quantity')) {
            throw new HttpResponseException(response()->json(['errors' => ['max'], 'messages' => ['Improve or build more '.Starshipyard::codeName()]], JsonResponse::HTTP_BAD_REQUEST));
        }

        /** @var Unit $unit */
        $unit = $district->units()
            ->where('type', $request->input('type'))->firstOrNew(['type' => $request->input('type')]);

        $quantity = $request->input('quantity');

        $unit->getGameRule()->consumeResources($district, $quantity);

        $unit->quantity += $quantity;
        $district->units()->save($unit);

        return new ApiUnitResource($unit);
    }

    /**
     * @TODO implement researches ?
     * @return ApiUnitResource[]
     */
    public function getUnits(int $id): array
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        $unitsInDistrict = $district->units()
            ->select('type', 'quantity')
            ->get();

        return array_map(function ($unitFqdn) use ($unitsInDistrict) {
            /** @var AbstractUnit $rule */
            $rule = new $unitFqdn;
            /** @var Unit $unit */
            $unit = $unitsInDistrict
                ->where('type', $rule::codeName())
                ->first(
                    null,
                    new Unit(['type' => $rule::codeName(), 'quantity' => 0])
                );

            return new ApiUnitResource($unit);
        }, GameService::UNITS);
    }

    public function sendFleet(int $id, UnitsSelection $request): ApiFleetResource
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        $stargate = Stargate::query()
            ->where('x', $request->input('x'))
            ->where('y', $request->input('y'))
            ->firstOrFail();

        DB::beginTransaction();
        $fleet = new Fleet([
            'district_id' => $district->id,
            'stargate_id' => $stargate->id,
        ]);
        $fleet->save();

        $district_units = $district->units()->get(['id', 'quantity', 'type']);

        foreach ($request->input('units') as $unitName => $quantity) {
            $district_unit = $district_units->where('type', $unitName)->first();
            if (null == $district_unit || $district_unit->quantity < $request->input('units')[$unitName]) {
                DB::rollback();
                abort(
                    Response::HTTP_BAD_REQUEST,
                    "You are trying to send {$request->input('units')[$unitName]}, you don't have enough of them."
                );
            }
            $district_unit->quantity -= $request->input('units')[$unitName];
            $district_unit->save();

            $fleet->units()->save(new Unit([
                'type' => $unitName,
                'quantity' => $quantity,
            ]));
        }

        GameRulesFleet::consumeResource($fleet);

        DB::commit();

        return new ApiFleetResource($fleet);
    }

    public function getDistrictResourcesData(int $id): array
    {
        return DistrictResources::getArray($id);
    }

    public function showBuilding(int $id, BuildingSelection $request): Building
    {
        /** @var DistrictField $field */
        $field = DistrictField::query()
            ->where('district_id', $id)
            ->whereNotNull('level')
            ->firstOrNew($request->validated(), ['level' => 0]);
        $field->district_id = $id;

        return new Building($field);
    }

    public function getBattleSummaries(int $id): BattleSummariesResource
    {
        /** @var District $district */
        $district = Auth::user()->districts()->findOrFail($id);

        return new BattleSummariesResource($district->battleSummaries()->get());
    }

    public function getFleets(int $id): JsonResponse
    {
        $district = Auth::user()->districts()->findOrFail($id);
        $fleets = Fleet::query()->where('district_id', $id)->get();
        $response = [];
        $fleets->each(function (Fleet $fleet) use (&$response): void {
            array_push($response, [
                "to" => $fleet->to->color,
                "departure_time" => $fleet->created_at,
                "travelTime" => $fleet->getTravelTime(),
                "units" => $fleet->units()->select(['type', 'quantity'])->get(),
            ]);
        });
        return new JsonResponse($response);
    }
}
