<?php

namespace App\Http\Controllers;

use App\Events\ChatMessageCreated;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    public function chat(Request $request)
    {
        $request->validate([
            'message' => ['required', 'string'],
            'channel' => ['required', 'in:planet'],
        ]);

        ChatMessageCreated::dispatch(
            $request->user()->username,
            $request->input('channel'),
            $request->input('message')
        );

        return response()->noContent();
    }
}
