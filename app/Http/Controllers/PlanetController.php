<?php

namespace App\Http\Controllers;

use App\GameRules\Planet;
use App\Http\Requests\SelectPlanetField;
use App\Http\Resources\PlanetField;
use App\Models\District;
use Illuminate\Support\Collection;

class PlanetController extends Controller
{
    /**
     * @return Collection<PlanetField>
     */
    public function index(): Collection
    {
        return PlanetField::collection(
            Planet::getFields()
        )->groupBy(['x', 'y']);
    }

    public function getDistanceFromDistrict(SelectPlanetField $request): \Illuminate\Http\JsonResponse
    {
        /** @var District $district */
        $district = District::query()->find($request->input('from'));

        return response()->json(Planet::getDistanceBetweenFields(
            $district->x,
            $district->y,
            $request->input('x'),
            $request->input('y')
        ));
    }
}
