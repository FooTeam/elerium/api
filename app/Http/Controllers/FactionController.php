<?php

namespace App\Http\Controllers;

use App\GameRules\Planet;
use App\Http\Requests\SelectPlanetField;
use App\Http\Resources\Factions;
use App\Http\Resources\PlanetField;
use App\Models\District;
use App\Models\Faction;
use Illuminate\Support\Collection;

class FactionController extends Controller
{
    public function getPoint():  Factions
    {
        $factions = Faction::query()->get();
        return new Factions($factions);
    }
}
