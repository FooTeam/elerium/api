<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotPassword;
use App\Http\Requests\Register;
use App\Http\Requests\ResetPassword;
use App\Models\Faction;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function checkOpenRegistration(): JsonResponse
    {
        $closedReason = $this->getClosedReason();
        if (null !== $closedReason) {
            return response()->json(['open' => false, 'reason' => $closedReason]);
        }

        return response()->json(['open' => true]);
    }

    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('username', 'password');

        if (! Auth::attempt($credentials)) {
            return response()->json('Unauthorized', 401);
        }

        return response()->json(Auth::user());
    }

    public function register(Register $request): JsonResponse
    {
        $closedReason = $this->getClosedReason();
        if (null !== $closedReason) {
            return response()->json([
                'open' => false,
                'reason' => $closedReason,
            ])->setStatusCode(503);
        }

        $validated = $request->validated();

        $faction = array_diff(Faction::NAMES, User::query()
            ->latest()
            ->limit(2)
            ->select('faction')
            ->pluck('faction')->toArray());

        DB::transaction(function () use ($validated, $faction): void {
            $user = new User;
            $user->username = $validated['username'];
            $user->faction = reset($faction);
            $user->password = $validated['password'];
            $user->email = $validated['email'] ?? null;
            $user->save();
        });

        Cache::increment('player-count');

        return response()->json('registered');
    }

    public function forgotPassword(ForgotPassword $request): JsonResponse
    {
        $validated = $request->validated();

        $status = Password::sendResetLink(['email' => $validated['email']]);
        if (PASSWORD::INVALID_USER === $status) {
            $status = PASSWORD::RESET_LINK_SENT;
        }

        $code = [
            PASSWORD::RESET_LINK_SENT => 200,
            PASSWORD::RESET_THROTTLED => 421,
        ][$status];

        return response()->json(['email' => __($status)], $code);
    }

    public function resetPassword(ResetPassword $request): JsonResponse
    {
        $validated = $request->validated();

        $status = Password::reset(
            $validated,
            function ($user, $password): void {
                $user->forceFill([
                    'password' => $password,
                ])->save();

                $user->setRememberToken(Str::random(60));

                event(new PasswordReset($user));
            }
        );

        if (Password::PASSWORD_RESET === $status) {
            return response()->json(__($status));
        }

        return response()->json(['errors' => ['message' => __($status)]], 400);
    }

    /**
     * @return string|null reason if closed, null if open
     */
    private function getClosedReason(): ?string
    {
        if (env('CLOSED', true)) {
            return env('CLOSED_REASON', 'Unknown reason');
        }

        if ($this->getPlayerCount() >= +env('MAX_NUMBER_PLAYERS', 100)) {
            return 'The server is full';
        }

        return null;
    }

    private function getPlayerCount(): int
    {
        return Cache::remember('player-count', now()->addDay(), fn (): int => User::query()->count('id'));
    }
}
