<?php

namespace App\Http\Controllers;

use App\GameRules\GameService;
use App\GameRules\Researches\AbstractResearch;
use App\Http\Requests\StartResearch;
use App\Http\Resources\Research as ResearchResource;
use App\Models\Research;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ResearchController extends Controller
{
    private ?Collection $userResearches;

    /**
     * List all researches.
     */
    public function index(GameService $gameService)//: JsonResponse
    {
        $this->setUserResearches();

        return new JsonResponse($this->loopOnTree($gameService->getResearchTree()));
    }

    public function startResearch(StartResearch $request): ResearchResource
    {
        $district = Auth::user()->districts()->firstOrFail();

        /** @var AbstractResearch $rule */
        $className = AbstractResearch::getRuleFQDN($request->input('name'));
        $rule = new $className;
        $research = $rule->validateUnlocked()->createModel();

        $rule->consumeResources($district);

        Auth::user()->researches()->save($research);
        Research::forgetCache();

        return new ResearchResource($research);
    }

    private function setUserResearches(): void
    {
        $this->userResearches = Auth::user()->researches;
    }

    private function loopOnTree(ResearchResource $tree): ResearchResource
    {
        /** @var ?Research $research */
        $research = $this->userResearches->where('name', $tree->name)->first();

        // Research not started
        if (null === $research) {
            return $tree;
        }

        $resource = new ResearchResource($research);
        $unlocks = [];
        foreach ($tree->additional['unlocks'] as $unlock) {
            $unlocks[] = $this->loopOnTree($unlock);
            $resource->additional(['unlocks' => $unlocks]);
        }

        return $resource;
    }
}
