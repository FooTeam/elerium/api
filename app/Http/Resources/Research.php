<?php

namespace App\Http\Resources;

use App\GameRules\Researches\AbstractResearch;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @see \App\Http\Controllers\ResearchController::loopOnTree for collection
 */
class Research extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Research|\App\Models\Research $this */
        /** @var AbstractResearch $rule */
        $rule = $this->getGameRule();

        return array_merge(
            [
                'name' => $this->name,
                'needed_science' => $rule->neededScience(),
                'needed_elerium' => $rule->neededElerium(),
                'unlocked' => $this->exists,
            ],
            $this->additional,
        );
    }
}
