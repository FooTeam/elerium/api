<?php

namespace App\Http\Resources;

use App\Models\BattleSummary;
use Illuminate\Http\Resources\Json\JsonResource;

class BattleSummaries extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];
        $this->each(function (BattleSummary $summary) use (&$response): void {
            $units = [];
            $summary->units()->get()->each(function ($unit) use (&$units): void {
                $units[$unit->type] = $unit->quantity;
            });

            array_push($response, array_merge([
                'id' => $summary->id,
                'created_at' => $summary->created_at,
                'stargate' => $summary->location()->select(['id'])->first(),
                'win' => $summary->win
            ], $units));
        });

        return $response;
    }
}
