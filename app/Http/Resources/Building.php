<?php

namespace App\Http\Resources;

use App\GameRules\Buildings\AbstractBuilding;
use App\GameRules\Buildings\AbstractProductionBuilding;
use App\GameRules\Buildings\AbstractStorageBuilding;
use App\GameRules\Buildings\Starshipyard;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \App\Models\DistrictField resource
 */
class Building extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var AbstractBuilding $building */
        $building = $this->getGameRule();

        $details = [
            'name' => $this->value,
            'level' => $building->level,
            'up' => [
                'neededMetal' => $building->neededMetal(),
                'neededGraphite' => $building->neededGraphite(),
            ],
        ];
        if (method_exists($building, 'baseProduction')) {
            /* @var AbstractProductionBuilding $building */
            return array_merge($details, $this->getProductionDetails($building));
        }

        if (method_exists($building, 'baseCapacity')) {
            /* @var AbstractStorageBuilding $building */
            return array_merge($details, $this->getStorageDetails($building));
        }

        if ($building instanceof Starshipyard) {
            return array_merge($details, [
                'unitStorage' => $building->unitStorage(),
                'nextLevelStorage' => $building->unitStorage($building->level + 1)
            ]);
        }

        return $details;
    }

    private function getProductionDetails(AbstractProductionBuilding $building): array
    {
        $naturalsCount = $this->resource->getNaturalCount();

        return [
            'production' => [
                'base' => $building->baseProduction(),
                'naturals' => [
                    'number' => $naturalsCount,
                    'boost' => $building->getBoostPerNaturals(),
                ],
                'researchBoost' => $building->getResearchBoost(),
                'total' => $building->totalProduction($naturalsCount),
            ],
            'nextLevelProduction' => $building->totalProduction($naturalsCount, $building->level + 1),
        ];
    }

    private function getStorageDetails(AbstractStorageBuilding $building): array
    {
        $researchBoost = $building->getResearchBoost();

        return [
            'storage' => [
                'base' => $building->baseCapacity(),
                'researchBoost' => $researchBoost,
                'total' => $building->totalStorage(),
            ],
            'nextLevelStorage' => $building->totalStorage($building->level + 1),
        ];
    }
}
