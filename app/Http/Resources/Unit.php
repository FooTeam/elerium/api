<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \App\Models\Unit $resource
 */
class Unit extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $unit = $this->resource->getGameRule();

        return [
            'type' => $this->resource->type,
            'quantity' => $this->resource->quantity,
            'speed' => $unit->getTravelSpeed(),
            'defense' => $unit->getDefense(),
            'attack' => $unit->getAttack(),
            'build' => [
                'metal' => $unit->neededMetal(),
                'elerium' => $unit->neededMetal(),
                'paste' => $unit->neededPaste(),
            ],
        ];
    }
}
