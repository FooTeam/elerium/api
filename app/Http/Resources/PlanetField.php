<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanetField extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $isDistrict = \App\Models\District::class === get_class($this->resource);

        if (!$isDistrict) {
            $this->resource->evaluateFleets();
            $units = [];
            $this->resource->units()->get()->each(function ($unit) use (&$units): void {
                if (array_key_exists($unit->type, $units)) {
                    $units[$unit->type] += $unit->quantity;
                    return;
                }
                $units[$unit->type] = $unit->quantity;
            });
        }
        /** @var User|false $user */
        $user = $this->user_id ? User::query()->select(['username', 'faction'])->find($this->user_id) : false;
        
        return [
            'id' => $this->id,
            'x' => $this->x,
            'y' => $this->y,
            'owner' => $this->when($user, $user->username ?? null),
            'faction' => $this->when($user, $user->faction ?? null),
            'type' => $isDistrict ? 'district' : 'stargate',
            'name' => $this->when($isDistrict, $this->name),
            'color' => $this->when(! $isDistrict, $this->color),
            'units' => !$isDistrict ? $units : null,
        ];
    }
}
