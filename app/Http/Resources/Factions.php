<?php

namespace App\Http\Resources;

use App\Models\BattleSummary;
use App\Models\Faction;
use Illuminate\Http\Resources\Json\JsonResource;

class Factions extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];
        $this->each(function (Faction $faction) use (&$response): void {
            $faction->evaluatePoint();
            $faction->refresh;
            
            array_push($response, [
                'name' => $faction->name,
                'orange' => $faction->orange,
                'violet' => $faction->violet,
                'yellow' => $faction->yellow,
                'red' => $faction->red,
                'win' => $faction->win()
            ]);
        });

        return $response;
    }
}
