<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \App\Models\Fleet $resource
 */
class Fleet extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $units = [];
        $this->units()->get()->each(function ($unit) use (&$units): void {
            $units[$unit->type] = $unit->quantity;
        });

        return array_merge([
            "from" => $this->from()->select(['id'])->first(),
            "to" => $this->to()->select(['id'])->first(),
            "speed" => $this->getSpeed(),
            "travelTime" => round($this->getTravelTime())
        ], $units);
    }
}
