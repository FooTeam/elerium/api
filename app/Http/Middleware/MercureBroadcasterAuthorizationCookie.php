<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class MercureBroadcasterAuthorizationCookie
{
    public function handle(Request $request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        if (! method_exists($response, 'withCookie')) {
            return $response;
        }

        if ($request->hasCookie('mercureAuthorization')) {
            return $next($request);
        }

        return $response->withCookie($this->createCookie($request->user(), $request->secure()));
    }

    private function createCookie(User $user, bool $secure)
    {
        // Topic(s) this user has access to
        $subscriptions = array_map(
            fn (string $uri) => env('CLIENT_URL', 'https://elerium.footeam.fr/').$uri,
            [
                'chat/planet',
                'battle/'.$user->username,
                'victory'
                // 'chat/faction/'.$user->faction
            ]
        );

        $jwtConfiguration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText(config('broadcasting.connections.mercure.secret'))
        );

        $token = $jwtConfiguration->builder()
            ->withClaim('mercure', ['subscribe' => $subscriptions])
            ->getToken($jwtConfiguration->signer(), $jwtConfiguration->signingKey())
            ->toString();

        return Cookie::make(
            'mercureAuthorization',
            $token,
            15,
            '/.well-known/mercure',
            parse_url(env('CLIENT_URL'), PHP_URL_HOST),
            $secure,
            true
        );
    }
}
