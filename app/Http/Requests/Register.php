<?php

namespace App\Http\Requests;

class Register extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['alpha_dash', 'between:2,18', 'required', 'unique:users'],
            'password' => ['required', 'min:8'],
            'email' => ['nullable', 'email', 'unique:users'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return trans('validation-inline');
    }
}
