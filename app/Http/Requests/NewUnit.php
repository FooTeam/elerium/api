<?php

namespace App\Http\Requests;

use App\GameRules\GameService;
use Illuminate\Validation\Rule;

class NewUnit extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in(GameService::getCodeNames(GameService::UNITS))],
            'quantity' => ['integer', 'required', 'min:1'],
        ];
    }
}
