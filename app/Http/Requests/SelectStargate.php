<?php

namespace App\Http\Requests;

use App\GameRules\Planet;

class SelectStargate extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'x' => ['required', 'integer', 'between:0,'.Planet::SIZE],
            'y' => ['required', 'integer', 'between:0,'.Planet::SIZE],
        ];
    }
}
