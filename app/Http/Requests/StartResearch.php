<?php

namespace App\Http\Requests;

use App\GameRules\GameService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StartResearch extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                Rule::in(GameService::getResearchesCodeNames()),
                Rule::notIn(Auth::user()->researches()->pluck('name')->toArray()),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'name.not_in' => __('research.already_started'),
        ];
    }
}
