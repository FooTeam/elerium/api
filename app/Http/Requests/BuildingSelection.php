<?php

namespace App\Http\Requests;

use App\GameRules\GameService;
use Illuminate\Validation\Rule;

class BuildingSelection extends SelectDistrictField
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'value' => [
                'required',
                Rule::in(GameService::getDistrictFieldsCodeNames()),
            ],
        ]);
    }
}
