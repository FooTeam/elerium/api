<?php

namespace App\Http\Requests;

use App\GameRules\Planet;
use App\Rules\IsPlayerDistrictId;

class SelectPlanetField extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => ['required', 'integer', 'between:0,'.Planet::SIZE, new IsPlayerDistrictId],
            'x' => ['required', 'integer', 'between:0,'.Planet::SIZE],
            'y' => ['required', 'integer', 'between:0,'.Planet::SIZE],
        ];
    }
}
