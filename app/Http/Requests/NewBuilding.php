<?php

namespace App\Http\Requests;

use App\GameRules\Buildings\DistrictCouncil;

class NewBuilding extends BuildingSelection
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['value'] = array_merge($rules['value'], ['not_in:'.DistrictCouncil::codeName()]);

        return $rules;
    }
}
