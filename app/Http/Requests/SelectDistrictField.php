<?php

namespace App\Http\Requests;

use App\Models\District;

class SelectDistrictField extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'x' => ['required', 'integer', 'between:0,'.District::WIDTH],
            'y' => ['required', 'integer', 'between:0,'.District::HEIGHT],
        ];
    }
}
