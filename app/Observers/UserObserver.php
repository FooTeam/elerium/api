<?php

namespace App\Observers;

use App\Models\District;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user): void
    {
        District::create($user);
    }

    /**
     * Handle the user "saving" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function saving(User $user): void
    {
        if (null === Hash::info($user->password)['algo']) {
            $user->password = Hash::make($user->password);
        }
    }
}
