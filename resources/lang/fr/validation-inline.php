<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'alpha_dash' => 'Ce champ doit contenir uniquement des lettres, des chiffres et des tirets.',
    'between' => [
        'string'  => 'Le texte doit contenir entre :min et :max caractères.',
    ],
    'confirmed' => 'Le champ de confirmation ne correspond pas.',
    'email' => 'Ce champ doit être une adresse email valide.',
    'min' => [
        'string'  => 'Le texte doit contenir au moins :min caractères.',
    ],
    'password' => 'Le mot de passe est incorrect',
    'required' => 'Ce champ est obligatoire.',
    'unique' => 'La valeur est déjà utilisée.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'username' => "identifiant",
        'email' => 'adresse email',
        'password' => 'mot de passe',
    ],
];
