<?php

return [
    'Coalfell',
    'Waterdeep',
    'Vale of Doom',
    'Braavos',
    'Thales',
    'Xynnar',
    'Duarudelf',
    'Eastford',
    'Darkwell',
    'Corralis',
    'Strong Oak',
    'Shadestown',
    'Camorrus',
    'Orsenna',
    'The Gale',
    'Yarrin',
    'Pavv',
    'Ashdrift',
    'Icemeet',
    'Tarrin',
    'Brilthor',
    'New New York',
    'Bay of Balar',
    'Rivermouth',
    'Pentos',
    'Aquarin',
    'Azmar',
    'Nogrod',
    'Lothlann',
    'Diaspar',
    'Eredluin',
    'Aenor’s Hill',
    'Vertrock',
    'Broken Shield',
    'Govannion',
    'Legolin',
    'Millstone',
    'Bullmar',
    'End’s Run',
    'Hogsfeet',
    'Shiny Ship',
    'Kortoli',
    'Hondo City',
    'Sundance',
    'Pyreacre',
    'Glaskerville',
    'Menzoberranzan',
    'Pella’s Wish',
    'The Last Battle',
    'Valley of Dusk',
    'Stone’s Throw',
    'Last Lock',
    'City of Fire',
    'Ecrin',
    'Westend',
    'Fangorn',
    'Ula’ree',
    'Lamorra',
    'Wintergale',
    'Rhaunar',
    'Besźel',
    'Suilwen',
    'Willowdale',
    'Xylar',
    'Harlen',
    'New Crobuzon',
    'Jarren’s Outpost',
    'Whiterun',
    'Begger’s Hole',
    'Hollyhead',
    'Mountmend',
    'Tarxia',
    'Xan’s Bequest',
    'Haran',
    'Rhuin',
    'Frostford',
    'Embertown',
    'Moonbright',
    'Firecrow',
    'Deathfall',
    'Cynosure',
    'Lochley',
    'Umbar',
    'Ubbin Falls',
    'Greenloft',
    'Pineland',
    'Briar Glen',
    'Northtown',
    'Pinnella Pass',
    'Squall’s End',
    'Kharé',
    'Three Portlands',
    'Firebend',
    'Solymbria',
    'Zolon',
    'Frostenvale',
    'Dry Gulch',
    'Merriwich',
    'Nandungorteb',
    'Stonewatch',
    'Aerilon',
    'Erede',
    'Opar',
    'Fool’s March',
    'Ship’s Haven',
    'Death’s Door',
    'Hwen',
    'Boatwright',
    'Darendale',
    'Dorlomin',
    'Red Hawk',
    'Sigil',
    'Iron Hills',
    'Hillfar',
    'Ironforge',
    'Carran',
    'Knife’s Edge',
    'Nearon',
    'Metouro',
    'Riverwind',
    'Neustern',
    'Tyrosh',
    'Duskendale',
    'Wolfden',
    'Swordbreak',
    'Northstead',
    'Ethermoor',
    'Oakensword',
    'Irragin',
    'Widow’s Peak',
    'Dragon’s Coast',
    'Greenflower',
    'Gulltown',
    'Cullfield ',
    'Doonatel',
    'Linesse',
    'Boaktis',
    'Seameet',
    'Ekaia',
    'Arrendyll',
    'Academy City',
    'Elbright',
    'Zumka',
    'East Armistice',
    'Norvos',
    'Fallenedge',
    'Queenstown',
    'Beleriand',
    'Aramoor',
    'Ered',
    'Leeside',
    'Doveshire',
    'Taur-in-duinath',
    'Adurant',
    'Othomae',
    'Qohor',
    'Neverwinter',
    'Alissar',
    'Eden’s Gulf',
    'Eldarmar',
    'Pran',
    'Orrinshire',
    'Lullin',
    'Amram',
    'Avaglade',
    'Crow’s Cry',
    'Black Hollow',
    'Aramanth',
    'Eribourne',
    'Myr',
    'Whiteridge',
    'Vindium',
    'Nargorthon',
    'Northpass',
    'Jongvale',
    'Dimbar',
    'Krondor',
    'Windrip',
    'Hull',
    'Pony Run',
    'Far Water',
    'Maglor’s Gap',
    'Ankh-Morpork',
    'New Cresthill',
    'Dorthonion',
    'Marren’s Eve',
    'Trudid',
    'Freevale',
    'Anfauglith',
    'Blue Field',
    'Greenglade',
    'Battlecliff',
    'Goldenleaf',
    'Yellowseed',
    'Lys',
    'Erast',
    'Lakeshore',
    'Zao Ying',
    'Wolfsrealm',
    'Old Ashton',
    'Horizon’s End',
    'Eastwatch',
    'Harsengaard',
    'Wellspring',
    'Ciudad Baranquilla',
    'Rhudaur',
    'Brickelwhyte',
    'Arnor',
    'Ys',
    'Amber',
    'Zeffari',
    'Saker Keep',
    'Quan Ma',
    'Lakestown',
    'Mahalaga',
    'Snowforge',
    'Westwend',
    'South Warren',
    'Three Streams',
    'Ramshorn',
    'Silverkeep',
    'Doveseal',
    'Doriath',
    'Oakheart',
    'Snowmelt',
    'Arlan’s Way',
    'Lorath',
    'Stahlstadt',
    'Port Blacksand',
    'Oar’s Rest',
    'Garen’s Well',
    'Ozryn',
    'Snake’s Canyon',
    'Frostfangs',
    'Superbia',
    'Veritas',
    'Ir',
    'Kara’s Vale',
    'Goldcrest',
    'Crydee',
    'Easthaven',
    'Nuxvar',
    'Bruinen',
    'Caprona',
    'Southern Vale',
    'Violl’s Garden',
    'Ul Qoma',
    'Yi Ti',
    'Free City of Greyhawk',
    'Wintervale',
];
