<?php

use App\Http\Controllers\DistrictController;
use App\Http\Controllers\FactionController;
use App\Http\Controllers\PlanetController;
use App\Http\Controllers\ResearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', fn (Request $request) => $request->user());

Route::group(['prefix' => 'districts'], function (): void {
    Route::get('', [DistrictController::class, 'getOwnDistricts']);
    Route::post('{id}/newBuilding', [DistrictController::class, 'addNewBuilding']);
    Route::post('{id}/upgrade', [DistrictController::class, 'upgradeBuilding']);
    Route::post('{id}/demolish', [DistrictController::class, 'demolish']);
    Route::post('{id}/newUnit', [DistrictController::class, 'addNewUnit']);
    Route::get('{id}/units', [DistrictController::class, 'getUnits']);
    Route::get('{id}/resources', [DistrictController::class, 'getDistrictResourcesData']);
    Route::get('{id}/building', [DistrictController::class, 'showBuilding']);
    Route::post('{id}/sendFleet', [DistrictController::class, 'sendFleet']);
    Route::get('{id}/getFleets', [DistrictController::class, 'getFleets']);
    Route::get('{id}/battleSummaries', [DistrictController::class, 'getBattleSummaries']);
});

Route::group(['prefix' => 'researches'], function (): void {
    Route::get('', [ResearchController::class, 'index']);
    Route::post('start', [ResearchController::class, 'startResearch']);
});

Route::group(['prefix' => 'planet'], function (): void {
    Route::get('/', [PlanetController::class, 'index']);
    Route::get('/getDistanceFromDistrict', [PlanetController::class, 'getDistanceFromDistrict']);
});

Route::post('chat', [\App\Http\Controllers\SocialController::class, 'chat']);

Route::group(['prefix'=> 'factions'], function (): void {
    Route::get('getPoint', [FactionController::class, 'getPoint']);
});
