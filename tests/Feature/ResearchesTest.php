<?php

namespace Tests\Feature;

use App\GameRules\Researches\EleriumAccelerator;
use App\GameRules\Researches\GroundScanner;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResearchesTest extends TestCase
{
    use RefreshDatabase;

    public function testInitialResearch(): void
    {
        $user = User::factory()->create();

        // Try start a locked research
        $this->actingAs($user)
            ->post('/api/researches/start', ['name' => GroundScanner::codeName()])
            ->assertJsonStructure(['errors', 'neededResearch']);
        $this->assertDatabaseCount('researches', 0);

        // Start the first research
        $this->actingAs($user)
            ->post('/api/researches/start', ['name' => EleriumAccelerator::codeName()]);
        $this->assertDatabaseCount('researches', 1);
        // and check the research tree.
        $rule = new EleriumAccelerator;
        $this->actingAs($user)
            ->get('/api/researches')
            ->assertJsonFragment([
                'name' => $rule->codeName(),
                'unlocked' => true,
                'unlocks' => [], // an array with unlocks as key, not necessarily an empty one
            ]);
    }
}
