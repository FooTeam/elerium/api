<?php

namespace Tests\Feature;

use App\GameRules\DistrictResources;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResourcesTest extends TestCase
{
    use RefreshDatabase;

    public function testInitialResources(): void
    {
        $resources = $this->getBaseResourcesArray();

        $user = User::factory()->create();

        $this->actingAs($user)
            ->get('/api/districts/1/resources')
            ->assertExactJson($resources);
    }

    public function testResourcesProduction(): void
    {
        $user = User::factory()->create();

        $this->travel(4)->minutes();

        $this->actingAs($user)
            ->get('/api/districts/1/resources')
            ->assertJsonFragment([
                'metal' => [
                    'max' => DistrictResources::BASE_STORAGE,
                    'production' => DistrictResources::BASE_PRODUCTION,
                    'value' => DistrictResources::BASE_PRODUCTION * 4,
                ],
            ]);
    }

    public function testResourcesMax(): void
    {
        $user = User::factory()->create();

        $this->travel(1)->years();

        $this->actingAs($user)
            ->get('/api/districts/1/resources')
            ->assertJsonFragment([
                'metal' => [
                    'max' => DistrictResources::BASE_STORAGE,
                    'production' => DistrictResources::BASE_PRODUCTION,
                    'value' => DistrictResources::BASE_STORAGE,
                ],
            ]);
    }

    private function getBaseResourcesArray(): array
    {
        return array_combine(
            DistrictResources::RESOURCES_NAMES,
            array_map(fn ($name) => [
                'max' => DistrictResources::BASE_STORAGE,
                'production' => DistrictResources::BASE_PRODUCTION,
                'value' => 0,
            ], DistrictResources::RESOURCES_NAMES)
        );
    }
}
