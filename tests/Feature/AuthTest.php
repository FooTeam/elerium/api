<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function getUserData($fields = ['password' => 'password', 'first_district' => 'first']): array
    {
        return array_merge(User::factory()->make()->toArray(), $fields);
    }

    public function testRegister(): void
    {
        $userData = $this->getUserData();
        $response = $this->post('/register', $userData);

        $this->assertSame(
            200,
            $response->getStatusCode(),
            sprintf('Registration failed with %s : %s', $response->getContent(), print_r($userData, true))
        );
    }

    public function testLogin(): void
    {
        $userData = $this->getUserData();
        $this->post('/register', $userData);

        $response = $this->post('/login', ['username' => $userData['username'], 'password' => 'password']);

        $this->assertSame(
            200,
            $response->getStatusCode(),
            sprintf('Login failed with %s', $response->getContent())
        );
    }

    public function testOpenServer(): void
    {
        $response = $this->get('/open-registrations');
        $this->assertSame(
            200,
            $response->getStatusCode()
        );
        $response->assertExactJson(['open' => true]);
    }

    public function testFullServer(): void
    {
        Cache::put('player-count', env('MAX_NUMBER_PLAYERS', 100));
        $this->get('/open-registrations')
            ->assertExactJson(['open' => false, 'reason' => 'The server is full']);

        Cache::put('player-count', env('MAX_NUMBER_PLAYERS', 100) + 1);
        $this->get('/open-registrations')
            ->assertExactJson(['open' => false, 'reason' => 'The server is full']);
    }

    public function testFullServerRegister(): void
    {
        Cache::put('player-count', env('MAX_NUMBER_PLAYERS', 100) - 1);

        // register the last user
        $this->testRegister();

        $response = $this->post('/register', $this->getUserData());
        $this->assertSame(
            503,
            $response->getStatusCode(),
            sprintf('Login should have failed with 503, got %s', $response->getStatusCode())
        );
    }

    public function testEvenlyDistributedFactions(): void
    {
        $this->post('/register', $this->getUserData());
        $this->post('/register', $this->getUserData());
        $this->post('/register', $this->getUserData());

        $this->assertDatabaseHas('users', [
            'faction' => 'federation',
        ]);
        $this->assertDatabaseHas('users', [
            'faction' => 'conglomerate',
        ]);
        $this->assertDatabaseHas('users', [
            'faction' => 'empire',
        ]);
    }
}
