<?php

namespace Tests\Unit;

use App\GameRules\Buildings\GraphiteMine;
use App\GameRules\Buildings\MetalMine;
use App\GameRules\Naturals\FertileSoil;
use App\GameRules\Naturals\GraphiteDeposit;
use App\GameRules\Naturals\MetalDeposit;
use App\GameRules\Planet;
use App\Models\District;
use App\Models\DistrictField;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Tests\TestCase;

class DistrictFieldTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public $seed = true;

    private District $district;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create();

        $district = new District();
        $district->name = $this->faker->city;
        do {
            $x = random_int(0, Planet::SIZE);
            $y = random_int(0, Planet::SIZE);
        } while (Planet::isOccupied($x, $y));
        $district->x = $x;
        $district->y = $y;

        $this->district = $user->districts()->save($district);
    }

    /**
     * @dataProvider provideCornerMetalMineSurroundedByDeposits
     * @dataProvider provideGraphiteMineSurroundedByDeposits
     * @dataProvider provideMetalMineSurroundedByOnlyTwoDeposits
     */
    public function testRelatedFieldNeighbors($expectedResult, DistrictField $input, string $related): void
    {
        [$input] = $this->district->fields()->saveMany([$input, ...$expectedResult]);

        foreach ($this->getNeighborFromDatabase($input, $input->getNeighbors($related)) as $neighbor) {
            $this->assertNotNull($neighbor);
            $this->assertEquals($related, $neighbor->value);
        }
    }

    public function provideCornerMetalMineSurroundedByDeposits()
    {
        $mine = new DistrictField([
            'x' => 0,
            'y' => 0,
            'value' => MetalMine::codeName(),
            'level' => 1,
        ]);

        $deposits = [
            new DistrictField([
                'x' => 1,
                'y' => 0,
                'value' => MetalDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 0,
                'y' => 1,
                'value' => MetalDeposit::codeName(),
                'level' => null,
            ]),
        ];

        /** @var \App\GameRules\Buildings\AbstractProductionBuilding */
        $gamerule = $mine->getGameRule();

        return [
            'Top left corner mine is surrounded by 2 deposits' => [
                $deposits,
                $mine,
                $gamerule->getRelatedNatural(),
            ],
        ];
    }

    public function provideGraphiteMineSurroundedByDeposits()
    {
        $mine = new DistrictField([
            'x' => 4,
            'y' => 5,
            'value' => GraphiteMine::codeName(),
            'level' => 1,
        ]);

        $deposits = [
            new DistrictField([
                'x' => 5,
                'y' => 5,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 4,
                'y' => 6,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 3,
                'y' => 6,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 3,
                'y' => 5,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 4,
                'y' => 4,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 5,
                'y' => 4,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
        ];

        /** @var \App\GameRules\Buildings\AbstractProductionBuilding */
        $gamerule = $mine->getGameRule();

        return [
            'Mine is surrounded by 6 deposits' => [
                $deposits,
                $mine,
                $gamerule->getRelatedNatural()
            ],
        ];
    }

    public function provideMetalMineSurroundedByOnlyTwoDeposits()
    {
        $mine = new DistrictField([
            'x' => 10,
            'y' => 6,
            'value' => MetalMine::codeName(),
            'level' => 1,
        ]);

        $deposits = [
            new DistrictField([
                'x' => 11,
                'y' => 6,
                'value' => MetalDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 10,
                'y' => 5,
                'value' => GraphiteDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 9,
                'y' => 7,
                'value' => MetalDeposit::codeName(),
                'level' => null,
            ]),
            new DistrictField([
                'x' => 11,
                'y' => 5,
                'value' => FertileSoil::codeName(),
                'level' => null,
            ]),
        ];

        /** @var \App\GameRules\Buildings\AbstractProductionBuilding */
        $gamerule = $mine->getGameRule();

        return [
            'Mine is sourrounded by only 2 deposits' => [
                $deposits,
                $mine,
                $gamerule->getRelatedNatural(),
            ],
        ];
    }

    private function getNeighborFromDatabase(DistrictField $input, Collection $neighbors)
    {
        foreach ($neighbors as $neighbor) {
            yield collect($neighbor->only($input->getKeyName()))
                ->reduce(
                    fn ($carry, $value, $key) => $carry->where($key, $value),
                    DistrictField::query()
                )
                ->first();
        }
    }
}
